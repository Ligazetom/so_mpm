#pragma once

#include <vector>
#include <limits>

class Mask
{
public:
	Mask();
	Mask(const Mask& mask);
	Mask(Mask&& mask);
	Mask& operator=(const Mask& mask);
	Mask& operator=(Mask&& mask);
	~Mask();

	void MakeCircularMask(int radius, double valueInside = 1., double valueOutside = -1.);
	const double* Ptr(int row = 0) const;
	double* Ptr(int row = 0);
	
	inline bool IsMaskSet() const { return static_cast<bool>(m_maskVals.size()); }
	inline int GetRowCount() const { return m_radius * 2 + 1; }
	inline int GetColumnCount() const { return m_radius * 2 + 1; }
	inline int GetRadius() const { return m_radius; }

	void SetRadius(int radius);
	void SetMaskValues(const std::vector<double>& mask);
	void SetMaskValues(std::vector<double>&& mask);
	void SetMaskValuesTo(double val);
	double GetMinimum() const;
	double GetMaximum() const;
	double GetPositiveMinimum() const;
	double GetNegativeMaximum() const;

private:
	int m_radius;
	std::vector<double*> m_rowPtrs;
	std::vector<double> m_maskVals;

	void SetupRowPtrs();
};
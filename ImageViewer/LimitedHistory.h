#pragma once

#include <deque>
#include <QDebug>

enum class LIMITED_HISTORY_EXCEPTION
{
	NO_MORE_HISTORY = 0,
	NO_MORE_FUTURE
};

template <class T>
class LimitedHistory
{
public:
	LimitedHistory(int maxSize = 10);

	int GetCurrentNumOfRecords() const;
	void SetMaxSizeLimit(int maxSize);
	void PushBack(const T& obj);
	T GetCurrent();
	T Undo();
	T Redo();
	bool DoesPreviousExist() const;
	bool DoesNextExist() const;

private:
	std::deque<T> m_history;
	int m_maxSize = 0;
	int m_currentIndex = -1;
};


template <class T>
LimitedHistory<T>::LimitedHistory(int maxSize)
{
	m_maxSize = maxSize;
}

template <class T>
int LimitedHistory<T>::GetCurrentNumOfRecords() const
{
	return m_history.size();
}

template <class T>
void LimitedHistory<T>::SetMaxSizeLimit(int maxSize)
{
	m_maxSize = maxSize;
}

template <class T>
void LimitedHistory<T>::PushBack(const T& obj)
{
	if (m_history.size() == m_maxSize && m_currentIndex == static_cast<int>(m_history.size()) - 1)
	{
		m_history.pop_front();
		m_history.push_back(obj);
	}
	else if (m_currentIndex < static_cast<int>(m_history.size()) - 1)		
	{
		int diff = static_cast<int>(m_history.size()) - 1 - m_currentIndex;

		for (int i = 0; i < diff; i++)
		{
			m_history.pop_back();
		}

		m_history.push_back(obj);
		m_currentIndex = static_cast<int>(m_history.size()) - 1;
	}
	else if (m_currentIndex < m_maxSize - 1)
	{
		m_history.push_back(obj);
		m_currentIndex++;
	}	
}

template <class T>
T LimitedHistory<T>::GetCurrent()
{
	return m_history[m_currentIndex];
}

template <class T>
T LimitedHistory<T>::Undo()
{
	if (m_currentIndex == 0) throw LIMITED_HISTORY_EXCEPTION::NO_MORE_HISTORY;

	m_currentIndex--;

	return m_history[m_currentIndex];
}

template <class T>
T LimitedHistory<T>::Redo()
{
	if (m_currentIndex == static_cast<int>(m_history.size()) - 1) throw LIMITED_HISTORY_EXCEPTION::NO_MORE_FUTURE;

	m_currentIndex++;

	return m_history[m_currentIndex];
}

template <class T>
bool LimitedHistory<T>::DoesPreviousExist() const
{
	if (m_currentIndex - 1 >= 0) return true;

	return false;
}

template <class T>
bool LimitedHistory<T>::DoesNextExist() const
{
	if (m_currentIndex + 1 <= static_cast<int>(m_history.size()) - 1) return true;

	return false;
}
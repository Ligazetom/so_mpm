#pragma once
#include <QtWidgets>
#include "Core.h"
#include "GeneralTypes.h"
#include "AnimationPlayer.h"
#include "AnimationManager.h"

class ViewerWidget :public QWidget {
	Q_OBJECT

public:
	ViewerWidget(QString viewerName, QSize imgSize, QWidget* parent = Q_NULLPTR);
	~ViewerWidget();
	
	void resizeWidget(QSize size);
	void ChangedMode(MODE mode);

	bool CanNextUndo();
	bool CanNextRedo();
	void Undo();
	void Redo();

	inline void setImage(const QImage& img) { m_imageOutput = m_core.SetImage(img); }
	inline const QImage* getImage() const { return m_imageOutput; };
	inline const QString& getName() { return name; }
	inline const void setName(QString newName) { name = newName; }
	inline int getImgWidth() { return m_imageOutput->width(); };
	inline int getImgHeight() { return m_imageOutput->height(); };


	//ANIMATION
	void Pause();
	void Stop();
	void SeekForward();
	void SeekBackward();
	void JumpToEnd();
	void JumpToStart();
	void PlayForward(bool loop, int fps);
	void PlayReverse(bool loop, int fps);
	
	void SelectAnimation(int index);
	inline AnimationPlayer* GetAnimationPlayer() { return &m_animationPlayer; }
	inline const AnimationManager* GetAnimationManager() const { return &m_animationManager; }

private:
	MODE m_mode = MODE::IMAGE_MODE;
	QString name = "";
	const QImage* const * m_output = nullptr;
	const QImage* m_imageOutput = nullptr;
	const QImage* m_animationOutput = nullptr;
	Core m_core;
	AnimationManager m_animationManager;
	AnimationPlayer m_animationPlayer;

	QRect Centralize(const QRect& env, const QImage& img) const;


public slots:
	void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;

	void OperationConfirmed();
	void RefreshFromHistory();

	void OperationConvolution(OperationConvolutionParams& params);
	void ThresholdBernsen(ThresholdBernsenParams& params);
	void ThresholdFLAT(ThresholdFLATParams& params);
	void ThresholdISODATA(ThresholdISODATAParams& params);
	void OperationFSHS(OperationFSHSParams& params);
	void OperationInvertColors(OperationInvertColorsParams& params);
	void OperationClear(OperationClearParams& params);
	void OperationLHEExplicit(OperationLHEExplicitParams& params);
	void OperationLHEImplicit(OperationLHEImplicitParams& params);
	void OperationPeronaMalikBasic(OperationPeronaMalikBasicParams& params);
	void OperationPeronaMalikRegularized(OperationPeronaMalikRegularizedParams& params);
	void OperationCurvatureFilter(OperationCurvatureFilterParams& params);
	void OperationGMCF(OperationGMCFParams& params);
	void OperationDistanceFunction(OperationDistanceFunctionParams& params);

private slots:
	void OnAnimationFrameChanged(int frameIndex);

signals:
	void OperationProcessed();
	void AnimationFrameChanged(int);
};
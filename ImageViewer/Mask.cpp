#include "Mask.h"

Mask::Mask()
{
	m_radius = 2;
}

Mask::~Mask()
{

}

Mask::Mask(const Mask& mask)
{
	m_radius = mask.m_radius;
	m_maskVals = mask.m_maskVals;
	
	SetupRowPtrs();
}

Mask::Mask(Mask&& mask)
{
	m_radius = mask.m_radius;
	m_maskVals = std::move(mask.m_maskVals);
	m_rowPtrs = std::move(mask.m_rowPtrs);
}

Mask& Mask::operator=(const Mask& mask)
{
	m_radius = mask.m_radius;
	m_maskVals = mask.m_maskVals;

	SetupRowPtrs();

	return *this;
}

Mask& Mask::operator=(Mask&& mask)
{
	if (this == &mask) return *this;
	m_radius = mask.m_radius;
	m_maskVals = std::move(mask.m_maskVals);
	m_rowPtrs = std::move(mask.m_rowPtrs);

	return *this;
}

void Mask::SetMaskValues(const std::vector<double>& mask)
{
	m_maskVals = mask;
	SetupRowPtrs();
}

void Mask::SetMaskValues(std::vector<double>&& mask)
{
	m_maskVals.swap(mask);
	SetupRowPtrs();
}

void Mask::SetupRowPtrs()
{
	m_rowPtrs.clear();
	m_rowPtrs.reserve(2 * static_cast<size_t>(m_radius) + 1);

	for (int i = 0; i < 2 * m_radius + 1; i++)
	{
		m_rowPtrs.emplace_back(&(m_maskVals[static_cast<size_t>(GetRowCount()) * i]));
	}
}

void Mask::SetRadius(int radius)
{
	m_radius = radius;
	m_maskVals.resize((2 * radius + 1) * (2 * radius + 1));
	SetupRowPtrs();
}

const double* Mask::Ptr(int row) const
{
	return m_rowPtrs[row];
}

double* Mask::Ptr(int row)
{
	return m_rowPtrs[row];
}

double Mask::GetMaximum() const
{
	double maximum = std::numeric_limits<double>::min();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] > maximum)
		{
			maximum = m_maskVals[i];
		}
	}

	return maximum;
}

double Mask::GetMinimum() const
{
	double minimum = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] < minimum)
		{
			minimum = m_maskVals[i];
		}
	}

	return minimum;
}

double Mask::GetPositiveMinimum() const
{
	double minimum = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] < 0)
		{
			continue;
		}

		if (m_maskVals[i] < minimum)
		{
			minimum = m_maskVals[i];
		}
	}

	return minimum;
}

double Mask::GetNegativeMaximum() const
{
	double maximum = std::numeric_limits<double>::min();

	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		if (m_maskVals[i] > 0)
		{
			continue;
		}

		if (m_maskVals[i] > maximum)
		{
			maximum = m_maskVals[i];
		}
	}

	return maximum;
}

void Mask::MakeCircularMask(int radius, double valueInside, double valueOutside)
{
	SetRadius(radius);

	for (size_t row = 0; row < m_rowPtrs.size(); row++)
	{
		int relativeRow = static_cast<int>(row) - m_radius;

		for (size_t col = 0; col < GetColumnCount(); col++)
		{
			int relativeColumn = static_cast<int>(col) - m_radius;

			if (relativeRow * relativeRow + relativeColumn * relativeColumn <= m_radius * m_radius)
			{
				*(m_rowPtrs[row] + col) = valueInside;
			}
			else
			{
				*(m_rowPtrs[row] + col) = valueOutside;
			}
		}
	}
}

void Mask::SetMaskValuesTo(double val)
{
	for (size_t i = 0; i < m_maskVals.size(); i++)
	{
		m_maskVals[i] = val;
	}
}
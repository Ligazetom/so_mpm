#pragma once

#include <qdialog.h>
#include "ui_LHEExplicitDialog.h"
#include <QMessageBox>
#include "GeneralTypes.h"

class LHEExplicitDialog : public QDialog
{
	Q_OBJECT

public:
	LHEExplicitDialog(QWidget* parent = Q_NULLPTR);

private:
	Ui::LHEExplicitDialog* ui;
	OperationLHEExplicitParams m_params;
	QMessageBox m_msgBox;

	void CopyParameters();
	bool IsStabilityConditionMet();
	void ShowMessageWarning();

private slots:
	void on_doubleSpinBoxTimeStep_valueChanged(double val);
	void on_doubleSpinBoxTime_valueChanged(double val);
	void on_checkBoxCreateAnimation_toggled(bool toggle);
	void on_spinBoxFrameDensity_valueChanged(int val);
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();

signals:
	void settingsAccepted(OperationLHEExplicitParams&);
};


#include "LHEImplicitDialog.h"

LHEImplicitDialog::LHEImplicitDialog(QWidget* parent) : QDialog(parent), ui(new Ui::LHEImplicitDialog)
{
	ui->setupUi(this);
}

void LHEImplicitDialog::CopyParameters()
{
	m_params.createAnimation = ui->checkBoxCreateAnimation->isChecked();
	m_params.saveEveryNthFrame = ui->spinBoxFrameDensity->value();
	m_params.animationName = ui->lineEditAnimationName->text();
	m_params.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_params.numOfSteps = static_cast<size_t>(ceil(ui->doubleSpinBoxTime->value() / m_params.timeStep));
	m_params.omega = ui->doubleSpinBoxOmega->value();
	m_params.tolerance = ui->doubleSpinBoxTolerance->value();
}

void LHEImplicitDialog::on_doubleSpinBoxTimeStep_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(ui->doubleSpinBoxTime->value() / val))));
}

void LHEImplicitDialog::on_doubleSpinBoxTime_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(val / ui->doubleSpinBoxTimeStep->value()))));
}

void LHEImplicitDialog::on_checkBoxCreateAnimation_toggled(bool toggle)
{
	ui->groupBoxAnimationSettings->setEnabled(toggle);
}

void LHEImplicitDialog::on_spinBoxFrameDensity_valueChanged(int val)
{
	QString text;

	switch (val)
	{
	case 1:
	{
		text = QString("st frame.");
		break;
	}
	case 2:
	{
		text = QString("nd frame.");
		break;
	}
	case 3:
	{
		text = QString("rd frame.");
		break;
	}
	default:
		text = QString("th frame.");
	}

	ui->labelFrameDensity->setText(text);
}

void LHEImplicitDialog::on_pushButtonOK_pressed()
{
	CopyParameters();
	emit settingsAccepted(m_params);
	accept();
}

void LHEImplicitDialog::on_pushButtonCancel_pressed()
{
	reject();
}

#pragma once

#include <qdialog.h>
#include "ui_GMCFDialog.h"
#include <QMessageBox>
#include "GeneralTypes.h"

class GMCFDialog : public QDialog
{
	Q_OBJECT

public:
	GMCFDialog(QWidget* parent = Q_NULLPTR);

private:
	Ui::GMCFDialog* ui;
	OperationGMCFParams m_params;
	QMessageBox m_msgBox;

	void CopyParameters();
	double GetEpsilon() const;

private slots:
	void on_doubleSpinBoxTimeStep_valueChanged(double val);
	void on_doubleSpinBoxTime_valueChanged(double val);
	void on_checkBoxCreateAnimation_toggled(bool toggle);
	void on_spinBoxFrameDensity_valueChanged(int val);
	void on_spinBoxSORIterationCheck_valueChanged(int val);
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();

signals:
	void settingsAccepted(OperationGMCFParams&);
};
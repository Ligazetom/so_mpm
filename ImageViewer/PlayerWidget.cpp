#include "PlayerWidget.h"

enum BUTTON_FLAG
{
	JUMP_BACKWARD = 0x01,
	SEEK_BACKWARD = 0x02,
	PLAY_REVERSE = 0x04,
	STOP = 0x08,
	PLAY_FORWARD = 0x10,
	SEEK_FORWARD = 0x20,
	JUMP_FORWARD = 0x40
};

PlayerWidget::PlayerWidget(QWidget* parent)
	: QWidget(parent), ui(new Ui::PlayerWidget)
{
	ui->setupUi(this);
}

void PlayerWidget::on_spinBoxCurrentFrame_valueChanged(int val)
{
	ui->horizontalSliderCurrentFrame->setValue(val);
	emit SpinBoxCurrentFrameValueChanged(val);
}

void PlayerWidget::on_horizontalSliderCurrentFrame_sliderMoved(int val)
{
	ui->spinBoxCurrentFrame->setValue(val);
	emit HorizontalSliderFrameValueChanged(val);
}

void PlayerWidget::ResetAnimationButtons()
{
	SetPlayerButtonsCheckedState(STOP);
}

void PlayerWidget::ResetAnimationControls()
{
	ui->spinBoxLength->setValue(0);
	ui->spinBoxSpeed->setValue(2);
	ui->spinBoxCurrentFrame->setValue(0);
	ui->checkBoxLoop->setChecked(false);
	ResetAnimationButtons();
}

void PlayerWidget::on_comboBoxAnimationName_currentIndexChanged(int val)
{
	emit ComboBoxAnimationNameIndexChanged(val);
}

void PlayerWidget::on_pushButtonJumpBackward_pressed()
{
	emit PushButtonJumpBackwardPressed();
}

void PlayerWidget::on_pushButtonJumpForward_pressed()
{
	emit PushButtonJumpForwardPressed();
}

void PlayerWidget::on_pushButtonPlayForward_pressed()
{
	if (ui->pushButtonPlayForward->isChecked())
	{
		emit PausePressed();
	}
	else
	{
		SetPlayerButtonsCheckedState(0);
		emit PushButtonPlayForwardPressed();
	}
}

void PlayerWidget::on_pushButtonPlayReverse_pressed()
{
	if (ui->pushButtonPlayReverse->isChecked())
	{
		emit PausePressed();
	}
	else
	{
		SetPlayerButtonsCheckedState(0);
		emit PushButtonPlayReversePressed();
	}
}

void PlayerWidget::on_pushButtonSeekBackward_pressed()
{
	SetPlayerButtonsCheckedState(0);
	emit PushButtonSeekBackwardPressed();
}

void PlayerWidget::on_pushButtonSeekForward_pressed()
{
	SetPlayerButtonsCheckedState(0);
	emit PushButtonSeekForwardPressed();
}

void PlayerWidget::on_pushButtonStop_pressed()
{
	SetPlayerButtonsCheckedState(0);
	emit PushButtonStopPressed();
}

void PlayerWidget::SetupComboBoxAnimationName(const AnimationManager* manager)
{
	ui->comboBoxAnimationName->clear();

	for (int i = 0; i < manager->GetNumOfAnimations(); i++)
	{
		ui->comboBoxAnimationName->addItem(manager->GetAnimation(i)->GetName());
	}

	if (manager->GetNumOfAnimations() == 0)
	{
		SetPlayerButtonsState(false);
	}
	else
	{
		SetPlayerButtonsState(true);
		ui->comboBoxAnimationName->setCurrentIndex(manager->GetNumOfAnimations() - 1);
	}
}

void PlayerWidget::SetPlayerButtonsState(bool enabled)
{
	ui->pushButtonJumpBackward->setEnabled(enabled);
	ui->pushButtonJumpForward->setEnabled(enabled);
	ui->pushButtonPlayForward->setEnabled(enabled);
	ui->pushButtonPlayReverse->setEnabled(enabled);
	ui->pushButtonSeekBackward->setEnabled(enabled);
	ui->pushButtonSeekForward->setEnabled(enabled);
	ui->pushButtonStop->setEnabled(enabled);
}

void PlayerWidget::SetupAnimationControls(const AnimationPlayer* player)
{
	ResetAnimationControls();

	if (player->HasAnimation())
	{
		ui->spinBoxLength->setValue(player->GetAnimationLength());
		ui->spinBoxCurrentFrame->setMaximum(player->GetAnimationLength());
		ui->horizontalSliderCurrentFrame->setMaximum(player->GetAnimationLength());
		ui->spinBoxSpeed->setValue(player->GetFps());
		ui->spinBoxCurrentFrame->setValue(player->GetCurrentFrameIndex());
		ui->checkBoxLoop->setChecked(player->IsInLoop());
	}
}

void PlayerWidget::SetPlayerButtonsCheckedState(int buttonsState)
{
	ui->pushButtonPlayForward->setChecked(PLAY_FORWARD & buttonsState);
	ui->pushButtonPlayReverse->setChecked(PLAY_REVERSE & buttonsState);
	ui->pushButtonSeekBackward->setChecked(SEEK_BACKWARD & buttonsState);
	ui->pushButtonSeekForward->setChecked(SEEK_FORWARD  & buttonsState);
	ui->pushButtonStop->setChecked(STOP & buttonsState);
}

void PlayerWidget::SetCurrentFrame(int frame)
{
	ui->horizontalSliderCurrentFrame->setValue(frame);
	ui->spinBoxCurrentFrame->setValue(frame);
}

void PlayerWidget::OnAnimationEnded()
{
	ResetAnimationButtons();
}
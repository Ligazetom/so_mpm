#include "GMCFDialog.h"

GMCFDialog::GMCFDialog(QWidget* parent) : QDialog(parent), ui(new Ui::GMCFDialog)
{
	ui->setupUi(this);
}

void GMCFDialog::CopyParameters()
{
	m_params.createAnimation = ui->checkBoxCreateAnimation->isChecked();
	m_params.saveEveryNthFrame = ui->spinBoxFrameDensity->value();
	m_params.animationName = ui->lineEditAnimationName->text();
	m_params.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_params.numOfSteps = static_cast<size_t>(ceil(ui->doubleSpinBoxTime->value() / m_params.timeStep));
	m_params.omega = ui->doubleSpinBoxOmega->value();
	m_params.tolerance = ui->doubleSpinBoxTolerance->value();
	m_params.epsilon = GetEpsilon();
	m_params.sorIterationResidualCheck = ui->spinBoxSORIterationCheck->value();
	m_params.visualizeEdgeDetector = m_params.createAnimation && ui->checkBoxVisualizeEdgeDetector->isChecked();
	m_params.K = ui->doubleSpinBoxK->value();
	m_params.numOfStepsImplicit = static_cast<size_t>(ceil(ui->doubleSpinBoxTimeImplicit->value() / m_params.timeStepImplicit));
	m_params.omegaImplicit = ui->doubleSpinBoxOmegaImplicit->value();
	m_params.timeStepImplicit = ui->doubleSpinBoxTimeStepImplicit->value();
	m_params.toleranceImplicit = ui->doubleSpinBoxToleranceImplicit->value();
}

double GMCFDialog::GetEpsilon() const
{
	double temp(ui->doubleSpinBoxEpsilon->value());

	for (int i = 0; i < abs(ui->spinBoxExponent->value()); i++)
	{
		temp /= 10.;
	}

	return temp;
}

void GMCFDialog::on_doubleSpinBoxTimeStep_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(ui->doubleSpinBoxTime->value() / val))));
}

void GMCFDialog::on_doubleSpinBoxTime_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(val / ui->doubleSpinBoxTimeStep->value()))));
}

void GMCFDialog::on_checkBoxCreateAnimation_toggled(bool toggle)
{
	ui->groupBoxAnimationSettings->setEnabled(toggle);
}

void GMCFDialog::on_spinBoxSORIterationCheck_valueChanged(int val)
{
	QString text;

	switch (val)
	{
	case 1:
	{
		text = QString("st iteration.");
		break;
	}
	case 2:
	{
		text = QString("nd iteration.");
		break;
	}
	case 3:
	{
		text = QString("rd iteration.");
		break;
	}
	default:
		text = QString("th iteration.");
	}

	ui->labelSORCheck->setText(text);
}

void GMCFDialog::on_spinBoxFrameDensity_valueChanged(int val)
{
	QString text;

	switch (val)
	{
	case 1:
	{
		text = QString("st frame.");
		break;
	}
	case 2:
	{
		text = QString("nd frame.");
		break;
	}
	case 3:
	{
		text = QString("rd frame.");
		break;
	}
	default:
		text = QString("th frame.");
	}

	ui->labelFrameDensity->setText(text);
}

void GMCFDialog::on_pushButtonOK_pressed()
{
	CopyParameters();
	emit settingsAccepted(m_params);
	accept();
}

void GMCFDialog::on_pushButtonCancel_pressed()
{
	reject();
}

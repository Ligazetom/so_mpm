#pragma once
#include <qobject.h>

#include "GeneralTypes.h"
#include "ThresholdBernsenDialog.h"
#include "MaskSettingsDialog.h"
#include "ThresholdFLATDialog.h"
#include "ViewerWidget.h"
#include "LHEExplicitDialog.h"
#include "LHEImplicitDialog.h"
#include "PeronaMalikBasicDialog.h"
#include "PeronaMalikRegularizedDialog.h"
#include "CurvatureFilterDialog.h"
#include "GMCFDialog.h"
#include "DistanceFunctionDialog.h"


class DialogManager :
	public QObject
{
	Q_OBJECT

public:
	DialogManager(QObject* parent = Q_NULLPTR);

	void MaskSettings();
	void OperationConvolution(ViewerWidget* connector);
	void ThresholdISODATA(ViewerWidget* connector);
	void ThresholdBernsen(ViewerWidget* connector);
	void ThresholdFLAT(ViewerWidget* connector);
	void InvertColors(ViewerWidget* connector);
	void FSHS(ViewerWidget* connector);
	void Clear(ViewerWidget* connector);
	void LHEExplicit(ViewerWidget* connector);
	void LHEImplicit(ViewerWidget* connector);
	void PeronaMalikBasic(ViewerWidget* connector);
	void PeronaMalikRegularized(ViewerWidget* connector);
	void CurvatureFilter(ViewerWidget* connector);
	void GMCF(ViewerWidget* connector);
	void DistanceFunction(ViewerWidget* connector);

	inline bool IsMaskSet() const { return m_maskSettingsDialog.IsMaskSet(); }

private:
	MaskSettingsDialog m_maskSettingsDialog;						//creating on stack to remember the mask values after exiting dialog
	ThresholdBernsenDialog* m_thresholdBernsenDialog = nullptr;
	ThresholdFLATDialog* m_thresholdFLATDialog = nullptr;
	LHEExplicitDialog* m_lheExplicitDialog = nullptr;
	LHEImplicitDialog* m_lheImplicitDialog = nullptr;
	PeronaMalikBasicDialog* m_peronaMalikBasicDialog = nullptr;
	PeronaMalikRegularizedDialog* m_peronaMalikRegularizedDialog = nullptr;
	CurvatureFilterDialog* m_curvatureFilterDialog = nullptr;
	GMCFDialog* m_GMCFDialog = nullptr;
	DistanceFunctionDialog* m_distanceFunctionDialog = nullptr;

private slots:
	
};


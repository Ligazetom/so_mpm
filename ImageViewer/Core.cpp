#include "Core.h"


Core::Core()
{
	Initialize();
}

Core::Core(const QImage& img)
{
	m_originalImg = img;
	m_showImg = img.copy();
	Initialize();
	PushShowedImgToHistory();
}

Core::~Core()
{
}

QImage* Core::GetShowImg()
{
	return &m_showImg;
}

const QImage* Core::SetImage(const QImage& img)
{
	m_originalImg = img;
	m_showImg = m_originalImg.copy();

	PushShowedImgToHistory();

	return &m_showImg;
}

const QImage* Core::NewImage(const QSize& imgSize)
{
	m_originalImg = QImage(imgSize, QImage::Format_Grayscale8);
	m_originalImg.fill(Qt::white);
	m_showImg = m_originalImg.copy();
	PushShowedImgToHistory();

	return &m_showImg;
}

const QImage& Core::GetOriginalImg() const
{
	return m_originalImg;
}

void Core::ComputeHistogram(const QImage* img)
{
	for (int i = 0; i < img->height(); i++)
	{
		for (int j = 0; j < img->width(); j++)
		{
			m_histogram.histogram[img->bits()[i * img->bytesPerLine() + j]]++;
		}
	}
}

void Core::ResetToOriginal()
{	
	m_showImg = m_originalImg.copy();
}

void Core::UpdateShowImage()
{
	m_resultImg.ConvertToQImage(&m_showImg);
}

void Core::ApplyOperation(OPERATION_TYPE type, void* params)
{
	switch (type)
	{
	case OPERATION_TYPE::DEFAULT: return;
	case OPERATION_TYPE::OPERATION_CONVOLUTION: 
	{
		OperationConvolution operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_FSHS:
	{
		OperationFSHS operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::THRESHOLD_BERNSEN:
	{
		ThresholdBernsen operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::THRESHOLD_FLAT:
	{
		ThresholdFLAT operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::THRESHOLD_ISODATA:
	{
		ThresholdISODATA operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_INVERT_COLORS:
	{
		OperationInvertColors operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_CLEAR:
	{
		OperationClear operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_LHE_EXPLICIT:
	{
		OperationLHEExplicit operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_LHE_IMPLICIT:
	{
		OperationLHEImplicit operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_PERONA_MALIK_BASIC:
	{
		OperationPeronaMalikBasic operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_PERONA_MALIK_REGULARIZED:
	{
		OperationPeronaMalikRegularized operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_CURVATURE_FILTER:
	{
		OperationCurvatureFilter operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_GMCF:
	{
		OperationGMCF operation;
		operation.Do(params);
		break;
	}
	case OPERATION_TYPE::OPERATION_DISTANCE_FUNCTION:
	{
		OperationDistanceFunction operation;
		operation.Do(params);
		break;
	}
	default:
		qDebug() << "Unknown error";
		return;
	}

	UpdateShowImage();
}

void Core::ApplyOperationConvolution(OperationConvolutionParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_CONVOLUTION, &params);
}

void Core::ApplyThresholdBernsen(ThresholdBernsenParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::THRESHOLD_BERNSEN, &params);
}

void Core::ApplyThresholdFLAT(ThresholdFLATParams& params)
{
	m_resultImg = NImage(m_history.GetCurrent());			//not one-time operation, need to keep "original" until OK was received
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::THRESHOLD_FLAT, &params);
}

void Core::ApplyThresholdISODATA(ThresholdISODATAParams& params)
{
	ComputeHistogram(&m_showImg);
	m_resultImg = NImage(m_showImg);	
	params.histogram = &m_histogram;
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::THRESHOLD_ISODATA, &params);
}

void Core::ApplyOperationFSHS(OperationFSHSParams& params)
{
	ComputeHistogram(&m_showImg);
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;
	params.histogram = &m_histogram;

	ApplyOperation(OPERATION_TYPE::OPERATION_FSHS, &params);
}

void Core::ApplyOperationInvertColors(OperationInvertColorsParams& params)
{
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_INVERT_COLORS, &params);
}

void Core::ApplyOperationClear(OperationClearParams& params)
{
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;
	params.clearValue = CLEAR_BACKGROUND_VALUE;

	ApplyOperation(OPERATION_TYPE::OPERATION_CLEAR, &params);
}

void Core::ApplyOperationLHEExplicit(OperationLHEExplicitParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_LHE_EXPLICIT, &params);
}

void Core::ApplyOperationLHEImplicit(OperationLHEImplicitParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_LHE_IMPLICIT, &params);
}

void Core::ApplyOperationPeronaMalikBasic(OperationPeronaMalikBasicParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_PERONA_MALIK_BASIC, &params);
}

void Core::ApplyOperationPeronaMalikRegularized(OperationPeronaMalikRegularizedParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_PERONA_MALIK_REGULARIZED, &params);
}

void Core::ApplyOperationCurvatureFilter(OperationCurvatureFilterParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_CURVATURE_FILTER, &params);
}

void Core::ApplyOperationGMCF(OperationGMCFParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);
	params.to = &m_resultImg;

	ApplyOperation(OPERATION_TYPE::OPERATION_GMCF, &params);
}

void Core::ApplyOperationDistanceFunction(OperationDistanceFunctionParams& params)
{
	params.from = &m_showImg;
	m_resultImg = NImage(m_showImg);

	ApplyOperation(OPERATION_TYPE::OPERATION_DISTANCE_FUNCTION, &params);
}

void Core::PushShowedImgToHistory()
{
	m_history.PushBack(m_showImg);
}

bool Core::Undo()
{
	try
	{
		m_showImg = m_history.Undo();
		
		return true;
	}
	catch (const LIMITED_HISTORY_EXCEPTION& e)
	{
		return false;
	}
}

bool Core::Redo()
{
	try
	{
		m_showImg = m_history.Redo();

		return true;
	}
	catch (const LIMITED_HISTORY_EXCEPTION& e)
	{
		return false;
	}
}

void Core::Initialize()
{
	m_history.SetMaxSizeLimit(MAX_UNDO_HISTORY);
}

bool Core::CanNextRedo()
{
	return m_history.DoesNextExist();
}

bool Core::CanNextUndo()
{
	return m_history.DoesPreviousExist();
}

void Core::Clear()
{
	m_showImg.fill(Qt::white);
}

void Core::RefreshFromHistory()
{
	m_showImg = m_history.GetCurrent();
}
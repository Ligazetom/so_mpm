#include "AnimationManager.h"

AnimationManager::AnimationManager(QObject* parent)
{
	
}

Animation* AnimationManager::NewAnimation(const QString& animationName)
{
	m_animations.push_back(Animation(animationName));

	return &m_animations.back();
}

void AnimationManager::PushAnimation(const Animation& animation)
{
	m_animations.push_back(animation);
}

const Animation* AnimationManager::GetAnimation(int index) const
{
	return &(m_animations[index]);
}
#include "AnimationPlayer.h"

AnimationPlayer::AnimationPlayer(QObject* parent)
{
	connect(&m_timer, SIGNAL(timeout()), this, SLOT(OnTimerTimeout()));
}

void AnimationPlayer::LoadAnimation(const Animation* animation)
{
	UnloadAnimation();

	m_animation = animation;
	m_currentFrameIndex = 0;
	m_animationLength = animation->Length();
	PlayFrame();
}

void AnimationPlayer::UnloadAnimation()
{
	m_animationLength = 0;
	m_animation = nullptr;
	m_currentFrameIndex = -1;
	
	if (m_timer.isActive()) StopTimer();
}

void AnimationPlayer::SetOutput(const QImage** img)
{
	m_output = img;
}

void AnimationPlayer::PlayForward(bool loop)
{
	m_loop = loop;
	m_isForward = true;

	if (!m_timer.isActive()) StartTimer(1. / m_fps);
	emit AnimationStarted();
}

void AnimationPlayer::PlayReverse(bool loop)
{
	m_loop = loop;
	m_isForward = false;

	if (m_currentFrameIndex == 0)
	{
		m_currentFrameIndex = m_animationLength - 1;
		emit FrameChanged(m_currentFrameIndex);
	}

	if (!m_timer.isActive()) StartTimer(1. / m_fps);
	emit AnimationStarted();
}

void AnimationPlayer::SetFramesPerSecond(size_t fps)
{
	m_fps = fps;
}

void AnimationPlayer::JumpToStart()
{
	m_currentFrameIndex = 0;
	emit FrameChanged(m_currentFrameIndex);
}

void AnimationPlayer::JumpToEnd()
{
	m_currentFrameIndex = m_animationLength - 1;
	emit FrameChanged(m_currentFrameIndex);
}

void AnimationPlayer::SeekForward()
{
	m_isForward = true;

	StopTimer();
	StartTimer(1. / (m_fps * 2));
}

void AnimationPlayer::SeekBackward()
{
	m_isForward = false;

	StopTimer();
	StartTimer(1. / (m_fps * 2));
}

void AnimationPlayer::Pause()
{
	StopTimer();
	emit AnimationPaused();
}

void AnimationPlayer::Stop()
{
	StopTimer();
	m_currentFrameIndex = 0;
	emit FrameChanged(m_currentFrameIndex);
	emit AnimationStopped();
}

void AnimationPlayer::SetFrameIndex(int frameIndex)
{
	m_currentFrameIndex = frameIndex;
	emit FrameChanged(m_currentFrameIndex);
	PlayFrame();
}

void AnimationPlayer::StopTimer()
{
	m_timer.stop();
}

void AnimationPlayer::StartTimer(double timeDiff)
{
	m_timer.setInterval(static_cast<int>(timeDiff * 1000));
	m_timer.start();
}

void AnimationPlayer::PlayFrame()
{
	*m_output = m_animation->GetFrame(m_currentFrameIndex);
}

void AnimationPlayer::IncrementFrame()
{
	if (m_currentFrameIndex == m_animationLength - 1)
	{
		if (m_loop)
		{
			m_currentFrameIndex = 0;
			emit FrameChanged(m_currentFrameIndex);
			PlayFrame();
		}
		else
		{
			Stop();
			emit AnimationEnded();
		}
	}
	else
	{
		m_currentFrameIndex++;
		emit FrameChanged(m_currentFrameIndex);
		PlayFrame();
	}
}

void AnimationPlayer::DecrementFrame()
{
	if (m_currentFrameIndex == 0)
	{
		if (m_loop)
		{
			m_currentFrameIndex = m_animationLength - 1;
			emit FrameChanged(m_currentFrameIndex);
			PlayFrame();
		}
		else
		{
			Stop();
			emit AnimationEnded();
		}
	}
	else
	{
		m_currentFrameIndex--;
		emit FrameChanged(m_currentFrameIndex);
		PlayFrame();
	}
}

void AnimationPlayer::OnTimerTimeout()
{
	m_isForward ? IncrementFrame() : DecrementFrame();
}
#include "ImageViewer.h"

ImageViewer::ImageViewer(QWidget* parent)
	: QMainWindow(parent), ui(new Ui::ImageViewerClass)
{
	ui->setupUi(this);
	m_playerWidget = new PlayerWidget(this);
	ui->verticalLayout_5->insertWidget(0, m_playerWidget);

	m_playerWidget->hide();

	connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(OnTabsSwitched(int)));


	connect(m_playerWidget, SIGNAL(ComboBoxAnimationNameIndexChanged(int)), this, SLOT(OnAnimationChanged(int)));
	connect(m_playerWidget, SIGNAL(SpinBoxCurrentFrameValueChanged(int)), this, SLOT(OnUserFrameChanged(int)));
	connect(m_playerWidget, SIGNAL(HorizontalSliderFrameValueChanged(int)), this, SLOT(OnUserFrameChanged(int)));
	connect(m_playerWidget, SIGNAL(PushButtonJumpForwardPressed()), this, SLOT(OnJumpForward()));
	connect(m_playerWidget, SIGNAL(PushButtonJumpBackwardPressed()), this, SLOT(OnJumpBackward()));
	connect(m_playerWidget, SIGNAL(PushButtonPlayForwardPressed()), this, SLOT(OnPlayForward()));
	connect(m_playerWidget, SIGNAL(PushButtonPlayReversePressed()), this, SLOT(OnPlayReverse()));
	connect(m_playerWidget, SIGNAL(PushButtonSeekForwardPressed()), this, SLOT(OnSeekForward()));
	connect(m_playerWidget, SIGNAL(PushButtonSeekBackwardPressed()), this, SLOT(OnSeekBackward()));
	connect(m_playerWidget, SIGNAL(PushButtonStopPressed()), this, SLOT(OnStop()));
	connect(m_playerWidget, SIGNAL(PausePressed()), this, SLOT(OnPause()));
}

//ViewerWidget functions
ViewerWidget* ImageViewer::getViewerWidget(int tabId)
{
	QScrollArea* s = static_cast<QScrollArea*>(ui->tabWidget->widget(tabId));
	if (s) {
		ViewerWidget* vW = static_cast<ViewerWidget*>(s->widget());
		return vW;
	}
	return nullptr;
}
ViewerWidget* ImageViewer::getCurrentViewerWidget()
{
	return getViewerWidget(ui->tabWidget->currentIndex());
}

// Event filters
bool ImageViewer::eventFilter(QObject* obj, QEvent* event)
{
	if (obj->objectName() == "ViewerWidget") {
		return ViewerWidgetEventFilter(obj, event);
	}
	return false;
}

//ViewerWidget Events
bool ImageViewer::ViewerWidgetEventFilter(QObject* obj, QEvent* event)
{
	ViewerWidget* w = static_cast<ViewerWidget*>(obj);

	if (!w) {
		return false;
	}

	switch (event->type())
	{
	case QEvent::Wheel: ViewerWidgetWheel(w, event); break;
	}

	return QObject::eventFilter(obj, event);
}


void ImageViewer::ViewerWidgetWheel(ViewerWidget* w, QEvent* event)
{
	QWheelEvent* wheelEvent = static_cast<QWheelEvent*>(event);
}

//ImageViewer Events
void ImageViewer::closeEvent(QCloseEvent* event)
{
	if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation", "Are you sure you want to exit?", QMessageBox::Yes | QMessageBox::No))
	{
		event->accept();
	}
	else {
		event->ignore();
	}
}

//Image functions
void ImageViewer::openNewTabForImg(ViewerWidget* vW)
{
	QScrollArea* scrollArea = new QScrollArea;
	scrollArea->setObjectName("QScrollArea");
	scrollArea->setWidget(vW);

	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidgetResizable(true);
	scrollArea->installEventFilter(this);
	scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

	vW->setObjectName("ViewerWidget");
	vW->installEventFilter(this);

	connect(vW, SIGNAL(OperationProcessed()), this, SLOT(HandleAfterOperationSetup()));
	connect(vW, SIGNAL(AnimationFrameChanged(int)), this, SLOT(OnAnimationFrameChanged(int)));
	connect(vW->GetAnimationPlayer(), SIGNAL(FrameChanged(int)), this, SLOT(OnAnimationFrameChanged(int)));
	connect(vW->GetAnimationPlayer(), SIGNAL(AnimationEnded()), m_playerWidget, SLOT(OnAnimationEnded()));
	
	QString name = vW->getName();

	ui->tabWidget->addTab(scrollArea, name);
}

bool ImageViewer::openImage(QString filename)
{
	if (filename == "") return false;

	QFileInfo fi(filename);

	QString name = fi.baseName();
	openNewTabForImg(new ViewerWidget(name, QSize(0, 0)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

	ViewerWidget* w = getCurrentViewerWidget();

	QImage loadedImg(filename);

	w->setImage(loadedImg);
	w->ChangedMode(MODE::IMAGE_MODE);

	return !loadedImg.isNull();
}

bool ImageViewer::saveImage(QString filename)
{
	QFileInfo fi(filename);
	QString extension = fi.completeSuffix();
	ViewerWidget* w = getCurrentViewerWidget();

	const QImage* img = w->getImage();
	return img->save(filename, extension.toStdString().c_str());
}

//Slots

//Tabs slots
void ImageViewer::on_tabWidget_tabCloseRequested(int tabId)
{
	ViewerWidget* vW = getViewerWidget(tabId);
	vW->deleteLater();
	ui->tabWidget->removeTab(tabId);
}

void ImageViewer::on_actionRename_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	ViewerWidget* w = getCurrentViewerWidget();
	bool ok;
	QString text = QInputDialog::getText(this, QString("Rename"), tr("Image name:"), QLineEdit::Normal, w->getName(), &ok);
	if (ok && !text.trimmed().isEmpty())
	{
		w->setName(text);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), text);
	}
}

//Image slots
void ImageViewer::on_actionNew_triggered()
{
	newImgDialog = new NewImageDialog(this);
	connect(newImgDialog, SIGNAL(accepted()), this, SLOT(newImageAccepted()));
	newImgDialog->exec();
	newImgDialog->deleteLater();
}

void ImageViewer::newImageAccepted()
{
	NewImageDialog* newImgDialog = static_cast<NewImageDialog*>(sender());

	int width = newImgDialog->getWidth();
	int height = newImgDialog->getHeight();
	QString name = newImgDialog->getName();
	openNewTabForImg(new ViewerWidget(name, QSize(width, height)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
}

void ImageViewer::on_actionOpen_triggered()
{
	QString folder = settings.value("folder_img_load_path", "").toString();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getOpenFileName(this, "Load image", folder, fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());

	if (!openImage(fileName)) {
		m_msgBox.setText("Unable to open image.");
		m_msgBox.setIcon(QMessageBox::Warning);
		m_msgBox.exec();
	}	
}

void ImageViewer::InformNoImageOpened()
{
	m_msgBox.setText("No image is opened.");
	m_msgBox.setIcon(QMessageBox::Information);
	m_msgBox.exec();
}

void ImageViewer::on_actionSave_as_triggered()
{
	if (!isImgOpened()) {
		m_msgBox.setText("No image to save.");
		m_msgBox.setIcon(QMessageBox::Information);
		m_msgBox.exec();
		return;
	}
	QString folder = settings.value("folder_img_save_path", "").toString();

	ViewerWidget* w = getCurrentViewerWidget();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getSaveFileName(this, "Save image", folder + "/" + w->getName(), fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_save_path", fi.absoluteDir().absolutePath());

	if (!saveImage(fileName)) {
		m_msgBox.setText("Unable to save image.");
		m_msgBox.setIcon(QMessageBox::Warning);
		m_msgBox.exec();
	}
	else {
		m_msgBox.setText(QString("File %1 saved.").arg(fileName));
		m_msgBox.setIcon(QMessageBox::Information);
		m_msgBox.exec();
	}
}

void ImageViewer::on_actionClear_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}
	
	m_dialogManager.Clear(getCurrentViewerWidget());
}

void ImageViewer::on_actionMask_settings_triggered()
{
	m_dialogManager.MaskSettings();
}

void ImageViewer::on_actionInvert_colors_triggered() {
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.InvertColors(getCurrentViewerWidget());
}

void ImageViewer::on_actionConvolutionApply_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	if (!m_dialogManager.IsMaskSet())
	{
		m_msgBox.setText("Mask needs to be set first!");
		m_msgBox.setIcon(QMessageBox::Information);
		m_msgBox.exec();

		return;
	}

	m_dialogManager.OperationConvolution(getCurrentViewerWidget());
}

void ImageViewer::on_actionThreshold_ISODATA_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.ThresholdISODATA(getCurrentViewerWidget());
}

void ImageViewer::on_actionThreshold_Bernsen_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.ThresholdBernsen(getCurrentViewerWidget());
}

void ImageViewer::on_actionThreshold_FLAT_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.ThresholdFLAT(getCurrentViewerWidget());
}

void ImageViewer::on_actionFSHS_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.FSHS(getCurrentViewerWidget());
}

void ImageViewer::on_actionLHE_Explicit_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.LHEExplicit(getCurrentViewerWidget());
}

void ImageViewer::on_actionLHE_Implicit_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.LHEImplicit(getCurrentViewerWidget());
}

void ImageViewer::on_actionPeronaMalikBasic_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.PeronaMalikBasic(getCurrentViewerWidget());
}

void ImageViewer::on_actionPeronaMalikRegularized_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.PeronaMalikRegularized(getCurrentViewerWidget());
}

void ImageViewer::on_actionCurvatureFilter_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.CurvatureFilter(getCurrentViewerWidget());
}

void ImageViewer::on_actionGMCF_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.GMCF(getCurrentViewerWidget());
}

void ImageViewer::on_actionDistanceFunction_triggered()
{
	if (!isImgOpened()) {
		InformNoImageOpened();
		return;
	}

	m_dialogManager.DistanceFunction(getCurrentViewerWidget());
}

void ImageViewer::on_actionUndo_triggered()
{
	getCurrentViewerWidget()->Undo();
	UpdateGUI(ui->tabWidget->currentIndex());
}

void ImageViewer::on_actionRedo_triggered()
{
	getCurrentViewerWidget()->Redo();
	UpdateGUI(ui->tabWidget->currentIndex());
}

void ImageViewer::OnTabsSwitched(int currentTabIndex)
{
	if (ui->tabWidget->count() < 1)
	{
		m_playerWidget->ClearComboBoxAnimationName();
		m_lastViewerWidget = nullptr;
		return;
	}

	if (m_lastViewerWidget != nullptr) {
		//Do anything with last viewerWidget	

		m_lastViewerWidget->Stop();

		///////////////////////////////////
	}

	m_lastViewerWidget = getViewerWidget(currentTabIndex);
	UpdateGUI(currentTabIndex);
}

void ImageViewer::UpdateGUI(int index)
{
	ui->actionRedo->setEnabled(getViewerWidget(index)->CanNextRedo());
	ui->actionUndo->setEnabled(getViewerWidget(index)->CanNextUndo());	

	UpdatePlayerWidget(index);
}

void ImageViewer::UpdatePlayerWidget(int index)
{
	ViewerWidget* vw = getViewerWidget(index);
	
	disconnect(m_playerWidget, SIGNAL(ComboBoxAnimationNameIndexChanged(int)), this, SLOT(OnAnimationChanged(int)));
	m_playerWidget->SetupComboBoxAnimationName(vw->GetAnimationManager());
	connect(m_playerWidget, SIGNAL(ComboBoxAnimationNameIndexChanged(int)), this, SLOT(OnAnimationChanged(int)));

	if (vw->GetAnimationManager()->GetNumOfAnimations() > 0) vw->SelectAnimation(vw->GetAnimationManager()->GetNumOfAnimations() - 1);
	
	m_playerWidget->SetupAnimationControls(vw->GetAnimationPlayer());	
}

void ImageViewer::HandleAfterOperationSetup()
{
	UpdateGUI(ui->tabWidget->currentIndex());
}

int ImageViewer::TabCount() const 
{
	return ui->tabWidget->count();
}

void ImageViewer::on_actionAnimation_Mode_triggered()
{
	m_playerWidget->show();
	ui->actionAnimation_Mode->setEnabled(false);
	ui->actionImage_Mode->setEnabled(true);

	ui->menuFile->setEnabled(false);
	ui->menuImage->setEnabled(false);
	ui->menuOperations->setEnabled(false);
	ui->menuAnimation->setEnabled(true);

	for (int i = 0; i < TabCount(); i++)
	{
		getViewerWidget(i)->ChangedMode(MODE::ANIMATION_MODE);
	}
}

void ImageViewer::on_actionImage_Mode_triggered()
{
	m_playerWidget->hide();
	ui->actionImage_Mode->setEnabled(false);
	ui->actionAnimation_Mode->setEnabled(true);

	ui->menuFile->setEnabled(true);
	ui->menuImage->setEnabled(true);
	ui->menuOperations->setEnabled(true);
	ui->menuAnimation->setEnabled(false);

	for (int i = 0; i < TabCount(); i++)
	{
		getViewerWidget(i)->ChangedMode(MODE::IMAGE_MODE);
	}
}

void ImageViewer::OnAnimationFrameChanged(int frameIndex)
{
	m_playerWidget->SetCurrentFrame(frameIndex + 1);
}

void ImageViewer::OnAnimationChanged(int index)
{
	if (TabCount() > 0)
	{
		getCurrentViewerWidget()->SelectAnimation(index);
		m_playerWidget->SetupAnimationControls(getCurrentViewerWidget()->GetAnimationPlayer());
	}
}

void ImageViewer::OnPlayForward()
{
	getCurrentViewerWidget()->PlayForward(m_playerWidget->Loop(), m_playerWidget->GetFps());
}

void ImageViewer::OnPlayReverse()
{
	getCurrentViewerWidget()->PlayReverse(m_playerWidget->Loop(), m_playerWidget->GetFps());
}

void ImageViewer::OnPause()
{
	getCurrentViewerWidget()->Pause();
}

void ImageViewer::OnStop()
{
	getCurrentViewerWidget()->Stop();
}

void ImageViewer::OnSeekForward()
{
	getCurrentViewerWidget()->SeekForward();
}

void ImageViewer::OnSeekBackward()
{
	getCurrentViewerWidget()->SeekBackward();
}

void ImageViewer::OnJumpForward()
{
	getCurrentViewerWidget()->JumpToEnd();
}

void ImageViewer::OnJumpBackward()
{
	getCurrentViewerWidget()->JumpToStart();
}

void ImageViewer::OnUserFrameChanged(int frame)
{
	getCurrentViewerWidget()->GetAnimationPlayer()->SetFrameIndex(frame - 1);
}
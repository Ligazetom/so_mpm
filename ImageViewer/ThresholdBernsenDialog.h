#pragma once
#include <qdialog.h>
#include "ui_ThresholdBernsenDialog.h"
#include "GeneralTypes.h"

class ThresholdBernsenDialog : public QDialog
{
	Q_OBJECT

public:
	ThresholdBernsenDialog(QWidget* parent = Q_NULLPTR);

	inline int GetRadius() const { return ui->spinBoxRadius->value(); }
	inline bool IsBackgroundDark() const { return static_cast<bool>(ui->comboBoxBackground->currentIndex()); }
	inline bool UseCircularMask() const { return ui->checkBoxUseCircularMask->isChecked(); }
	inline double GetMinimalContrast() const { return ui->doubleSpinBoxMinContrast->value(); }

private:
	Ui::ThresholdBernsenDialog* ui;	
	ThresholdBernsenParams m_params;

	void CopyParameters();

private slots:
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();

signals:
	void settingsAccepted(ThresholdBernsenParams&);
};


#pragma once

#include "Mask.h"
#include "NImage.h"
#include <QString>
#include "Animation.h"

enum class MODE
{
	IMAGE_MODE = 0,
	ANIMATION_MODE
};

struct Histogram
{
	size_t histogram[256] = {};
};

struct PixelGradients
{
	double east = 0.;
	double north = 0.;
	double west = 0.;
	double south = 0.;
};

enum class OPERATION_TYPE
{
	DEFAULT = -1,
	THRESHOLD_BERNSEN = 0,
	THRESHOLD_ISODATA,
	THRESHOLD_FLAT,
	OPERATION_CONVOLUTION,
	OPERATION_FSHS,
	OPERATION_INVERT_COLORS,
	OPERATION_CLEAR,
	OPERATION_LHE_EXPLICIT,
	OPERATION_LHE_IMPLICIT,
	OPERATION_PERONA_MALIK_BASIC,
	OPERATION_PERONA_MALIK_REGULARIZED,
	OPERATION_CURVATURE_FILTER,
	OPERATION_GMCF,
	OPERATION_DISTANCE_FUNCTION
};

struct DefaultParams
{
	OPERATION_TYPE type = OPERATION_TYPE::DEFAULT;
};

struct OperationClearParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_CLEAR;
	NImage* to = nullptr;
	double clearValue = 1.;
};

struct ThresholdBernsenParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::THRESHOLD_BERNSEN;
	const QImage* from = nullptr;
	NImage* to = nullptr;;
	int radius = 0;
	double minContrast = 0.3;
	bool isBackgroundDark = false;
	bool useCircularMask = true;
};

struct ThresholdISODATAParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::THRESHOLD_ISODATA;
	const Histogram* histogram = nullptr;
	NImage* to = nullptr;
};

struct ThresholdFLATParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::THRESHOLD_FLAT;
	uchar thresholdValue = 0;
	NImage* to = nullptr;
};

struct OperationFSHSParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_FSHS;
	const Histogram* histogram = nullptr;
	NImage* to = nullptr;
};

struct OperationConvolutionParams
{
	OperationConvolutionParams(const Mask& Mask) : mask(Mask) {};
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_CONVOLUTION;
	const QImage* from = nullptr;
	NImage* to = nullptr;
	const Mask& mask;
};

struct OperationInvertColorsParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_INVERT_COLORS;
	NImage* to = nullptr;
};

struct OperationLHEExplicitParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_LHE_EXPLICIT;
	const QImage* from = nullptr;
	NImage* to = nullptr;
	QString animationName = QString();
	Animation* animTo = nullptr;
	int saveEveryNthFrame = 0;
	double timeStep = 0;
	size_t numOfSteps = 0;
	bool createAnimation = true;
};

struct OperationLHEImplicitParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_LHE_IMPLICIT;
	const QImage* from = nullptr;
	NImage* to = nullptr;
	QString animationName = QString();
	Animation* animTo = nullptr;
	int saveEveryNthFrame = 0;
	size_t numOfSteps = 0;
	double timeStep = 0.;
	double tolerance = 1e-6;
	double omega = 1.;
	bool createAnimation = true;
};

struct OperationPeronaMalikBasicParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_PERONA_MALIK_BASIC;
	const QImage* from = nullptr;
	NImage* to = nullptr;
	QString animationName = QString();
	Animation* animTo = nullptr;
	Animation* edgeAnimTo = nullptr;
	int saveEveryNthFrame = 0;
	size_t numOfSteps = 0;
	double timeStep = 0.;
	double K = 1.;
	bool createAnimation = true;
	bool visualizeEdgeDetector = false;
};

struct OperationPeronaMalikRegularizedParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_PERONA_MALIK_REGULARIZED;
	const QImage* from = nullptr;
	NImage* to = nullptr;
	QString animationName = QString();
	Animation* animTo = nullptr;
	Animation* edgeAnimTo = nullptr;
	int saveEveryNthFrame = 0;
	size_t numOfSteps = 0;
	double timeStep = 0.;
	double tolerance = 1e-6;
	double omega = 1.;
	size_t numOfStepsImplicit = 0;
	double timeStepImplicit = 0.;
	double toleranceImplicit = 0.;
	double omegaImplicit = 1.;
	double K = 1.;
	bool createAnimation = true;
	bool visualizeEdgeDetector = false;
};

struct OperationCurvatureFilterParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_CURVATURE_FILTER;
	const QImage* from = nullptr;
	const NImage* Nfrom = nullptr;
	NImage* to = nullptr;
	QString animationName = QString();
	Animation* animTo = nullptr;
	Animation* edgeAnimTo = nullptr;
	int saveEveryNthFrame = 0;
	size_t numOfSteps = 0;
	double timeStep = 0.;
	double tolerance = 1e-6;
	double omega = 1.;
	double epsilon = 0.02;
	int sorIterationResidualCheck = 1;
	bool createAnimation = true;
	bool visualizeEdgeDetector = false;
};

struct OperationGMCFParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_GMCF;
	const QImage* from = nullptr;
	NImage* to = nullptr;
	QString animationName = QString();
	Animation* animTo = nullptr;
	Animation* edgeAnimTo = nullptr;
	int saveEveryNthFrame = 0;
	size_t numOfSteps = 0;
	double timeStep = 0.;
	double tolerance = 1e-6;
	double omega = 1.;
	double epsilon = 0.02;
	int sorIterationResidualCheck = 1;
	bool createAnimation = true;
	bool visualizeEdgeDetector = false;
	double K = 0.02;
	size_t numOfStepsImplicit = 1;
	double omegaImplicit = 1.1;
	double timeStepImplicit = 1.;
	double toleranceImplicit = 0.001;
};

struct OperationDistanceFunctionParams
{
	static constexpr OPERATION_TYPE type = OPERATION_TYPE::OPERATION_DISTANCE_FUNCTION;
	OperationCurvatureFilterParams params;
	const QImage* from = nullptr;
	int numOfIterations = 0;
};
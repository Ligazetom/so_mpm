#include "PeronaMalikRegularizedDialog.h"

PeronaMalikRegularizedDialog::PeronaMalikRegularizedDialog(QWidget* parent) : QDialog(parent), ui(new Ui::PeronaMalikRegularizedDialog)
{
	ui->setupUi(this);
}

void PeronaMalikRegularizedDialog::CopyParameters()
{
	m_params.createAnimation = ui->checkBoxCreateAnimation->isChecked();
	m_params.saveEveryNthFrame = ui->spinBoxFrameDensity->value();
	m_params.animationName = ui->lineEditAnimationName->text();
	m_params.visualizeEdgeDetector = m_params.createAnimation && ui->checkBoxVisualizeEdgeDetector->isChecked();

	m_params.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_params.numOfSteps = static_cast<size_t>(ceil(ui->doubleSpinBoxTime->value() / m_params.timeStep));	
	m_params.omega = ui->doubleSpinBoxOmega->value();
	m_params.tolerance = ui->doubleSpinBoxTolerance->value();

	m_params.K = ui->doubleSpinBoxK->value();

	m_params.timeStepImplicit = ui->doubleSpinBoxTimeStepImplicit->value();
	m_params.numOfStepsImplicit = static_cast<size_t>(ceil(ui->doubleSpinBoxTimeImplicit->value() / m_params.timeStepImplicit));	
	m_params.omegaImplicit = ui->doubleSpinBoxOmegaImplicit->value();
	m_params.toleranceImplicit = ui->doubleSpinBoxToleranceImplicit->value();
}

void PeronaMalikRegularizedDialog::on_doubleSpinBoxTimeStep_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(ui->doubleSpinBoxTime->value() / val))));
}

void PeronaMalikRegularizedDialog::on_doubleSpinBoxTime_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(val / ui->doubleSpinBoxTimeStep->value()))));
}

void PeronaMalikRegularizedDialog::on_doubleSpinBoxTimeStepImplicit_valueChanged(double val)
{
	if (ui->pushButtonLockStep->isChecked())
	{
		ui->doubleSpinBoxTimeImplicit->setValue(val);
		ui->labelIterationCountImplicit->setText(QString("Iteration count: 1"));
	}
	else
	{
		ui->labelIterationCountImplicit->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(ui->doubleSpinBoxTime->value() / val))));
	}
}

void PeronaMalikRegularizedDialog::on_doubleSpinBoxTimeImplicit_valueChanged(double val)
{
	if (ui->pushButtonLockStep->isChecked())
	{
		ui->doubleSpinBoxTimeStepImplicit->setValue(val);
		ui->labelIterationCountImplicit->setText(QString("Iteration count: 1"));
	}
	else
	{
		ui->labelIterationCountImplicit->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(val / ui->doubleSpinBoxTimeStep->value()))));
	}	
}


void PeronaMalikRegularizedDialog::on_checkBoxCreateAnimation_toggled(bool toggle)
{
	ui->groupBoxAnimationSettings->setEnabled(toggle);
}

void PeronaMalikRegularizedDialog::on_spinBoxFrameDensity_valueChanged(int val)
{
	QString text;

	switch (val)
	{
	case 1:
	{
		text = QString("st frame.");
		break;
	}
	case 2:
	{
		text = QString("nd frame.");
		break;
	}
	case 3:
	{
		text = QString("rd frame.");
		break;
	}
	default:
		text = QString("th frame.");
	}

	ui->labelFrameDensity->setText(text);
}

void PeronaMalikRegularizedDialog::on_pushButtonOK_pressed()
{
	CopyParameters();
	emit settingsAccepted(m_params);
	accept();
}

void PeronaMalikRegularizedDialog::on_pushButtonCancel_pressed()
{
	reject();
}

#include   "ViewerWidget.h"

ViewerWidget::ViewerWidget(QString viewerName, QSize imgSize, QWidget* parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	setMouseTracking(true);
	name = viewerName;

	if (imgSize != QSize(0, 0)) {
		m_imageOutput = m_core.NewImage(imgSize);
		ChangedMode(MODE::IMAGE_MODE);
	}

	m_animationPlayer.SetOutput(&m_animationOutput);
	connect(&m_animationPlayer, SIGNAL(FrameChanged(int)), this, SLOT(OnAnimationFrameChanged(int)));
}
ViewerWidget::~ViewerWidget()
{
	this->disconnect();
}
void ViewerWidget::resizeWidget(QSize size)
{
	this->resize(size);
	this->setMinimumSize(size);
	this->setMaximumSize(size);
}

//Slots
void ViewerWidget::paintEvent(QPaintEvent* event)
{
	if (m_output == nullptr || *m_output == nullptr) return;

	QPainter painter(this);

	painter.drawImage(Centralize(event->rect(), **m_output), **m_output, (*m_output)->rect());
}

void ViewerWidget::OperationConvolution(OperationConvolutionParams& params)
{
	m_core.ApplyOperationConvolution(params);
	update();
}

void ViewerWidget::ThresholdBernsen(ThresholdBernsenParams& params)
{
	m_core.ApplyThresholdBernsen(params);
	update();
}

void ViewerWidget::ThresholdFLAT(ThresholdFLATParams& params)
{
	m_core.ApplyThresholdFLAT(params);
	update();
}

void ViewerWidget::ThresholdISODATA(ThresholdISODATAParams& params)
{
	m_core.ApplyThresholdISODATA(params);
	update();
}

void ViewerWidget::OperationFSHS(OperationFSHSParams& params)
{
	m_core.ApplyOperationFSHS(params);
	update();
}

void ViewerWidget::OperationInvertColors(OperationInvertColorsParams& params)
{
	m_core.ApplyOperationInvertColors(params);
	update();
}

void ViewerWidget::OperationClear(OperationClearParams& params)
{
	m_core.ApplyOperationClear(params);
	update();
}

void ViewerWidget::OperationLHEExplicit(OperationLHEExplicitParams& params)
{
	QString animationName;

	if (params.animationName.isEmpty())
	{
		animationName = QString("LHE Explicit %1").arg(m_animationManager.GetNumOfAnimations() + 1);
	}
	else
	{
		animationName = params.animationName;
	}

	if (params.createAnimation)
	{
		params.animTo = m_animationManager.NewAnimation(animationName);
	}

	m_core.ApplyOperationLHEExplicit(params);
	update();
}

void ViewerWidget::OperationLHEImplicit(OperationLHEImplicitParams& params)
{
	QString animationName;

	if (params.animationName.isEmpty())
	{
		animationName = QString("LHE Implicit %1").arg(m_animationManager.GetNumOfAnimations() + 1);
	}
	else
	{
		animationName = params.animationName;
	}

	if (params.createAnimation)
	{
		params.animTo = m_animationManager.NewAnimation(animationName);
	}

	m_core.ApplyOperationLHEImplicit(params);
	update();
}

void ViewerWidget::OperationPeronaMalikBasic(OperationPeronaMalikBasicParams& params)
{
	QString animationName;

	if (params.animationName.isEmpty())
	{
		animationName = QString("Perona-Malik Basic %1").arg(m_animationManager.GetNumOfAnimations() + 1);
	}
	else
	{
		animationName = params.animationName;
	}

	if (params.createAnimation)
	{
		params.animTo = m_animationManager.NewAnimation(animationName);

		if (params.visualizeEdgeDetector)
		{
			params.edgeAnimTo = m_animationManager.NewAnimation(animationName + " Edge detector");
			params.animTo = params.edgeAnimTo - 1;
		}
	}

	m_core.ApplyOperationPeronaMalikBasic(params);
	update();
}

void ViewerWidget::OperationPeronaMalikRegularized(OperationPeronaMalikRegularizedParams& params)
{
	QString animationName;

	if (params.animationName.isEmpty())
	{
		animationName = QString("Perona-Malik Regularized %1").arg(m_animationManager.GetNumOfAnimations() + 1);
	}
	else
	{
		animationName = params.animationName;
	}

	if (params.createAnimation)
	{
		params.animTo = m_animationManager.NewAnimation(animationName);

		if (params.visualizeEdgeDetector)
		{
			params.edgeAnimTo = m_animationManager.NewAnimation(animationName + " Edge detector");
			params.animTo = params.edgeAnimTo - 1;
		}
	}

	m_core.ApplyOperationPeronaMalikRegularized(params);
	update();
}

void ViewerWidget::OperationCurvatureFilter(OperationCurvatureFilterParams& params)
{
	QString animationName;

	if (params.animationName.isEmpty())
	{
		animationName = QString("Curvature filter %1").arg(m_animationManager.GetNumOfAnimations() + 1);
	}
	else
	{
		animationName = params.animationName;
	}

	if (params.createAnimation)
	{
		params.animTo = m_animationManager.NewAnimation(animationName);

		if (params.visualizeEdgeDetector)
		{
			params.edgeAnimTo = m_animationManager.NewAnimation(animationName + " Edge detector");
			params.animTo = params.edgeAnimTo - 1;
		}
	}

	m_core.ApplyOperationCurvatureFilter(params);
	update();
}

void ViewerWidget::OperationGMCF(OperationGMCFParams& params)
{
	QString animationName;

	if (params.animationName.isEmpty())
	{
		animationName = QString("GMCF %1").arg(m_animationManager.GetNumOfAnimations() + 1);
	}
	else
	{
		animationName = params.animationName;
	}

	if (params.createAnimation)
	{
		params.animTo = m_animationManager.NewAnimation(animationName);

		if (params.visualizeEdgeDetector)
		{
			params.edgeAnimTo = m_animationManager.NewAnimation(animationName + " Edge detector");
			params.animTo = params.edgeAnimTo - 1;
		}
	}

	m_core.ApplyOperationGMCF(params);
	update();
}

void ViewerWidget::OperationDistanceFunction(OperationDistanceFunctionParams& params)
{
	m_core.ApplyOperationDistanceFunction(params);
	update();
}

void ViewerWidget::OperationConfirmed()
{
	m_core.PushShowedImgToHistory();
	emit OperationProcessed();
}

void ViewerWidget::Undo()
{
	m_core.Undo();
	update();
}

void ViewerWidget::Redo()
{
	m_core.Redo();
	update();
}

bool ViewerWidget::CanNextUndo()
{
	return m_core.CanNextUndo();
}

bool ViewerWidget::CanNextRedo()
{
	return m_core.CanNextRedo();
}

QRect ViewerWidget::Centralize(const QRect& env, const QImage& img) const
{
	QPoint center;
	QPoint leftTop;
	QPoint rightBot;

	center.setX(env.x() + env.right() / 2);
	center.setY(env.y() + env.bottom() / 2);

	leftTop.setX(center.x() - img.width() / 2);
	leftTop.setY(center.y() - img.height() / 2);

	rightBot.setX(leftTop.x() + img.width());
	rightBot.setY(leftTop.y() + img.height());

	return QRect(leftTop, rightBot);
}

void ViewerWidget::RefreshFromHistory()
{
	m_core.RefreshFromHistory();
	update();
}

void ViewerWidget::ChangedMode(MODE mode)
{
	m_mode = mode;

	switch (mode)
	{
	case MODE::IMAGE_MODE:
	{
		m_output = &m_imageOutput;
		break;
	}
	case MODE::ANIMATION_MODE:
	{
		m_output = &m_animationOutput;
		break;
	}
	default:
	{
		m_output = &m_imageOutput;
	}
	}

	update();
}

void ViewerWidget::OnAnimationFrameChanged(int frameIndex)
{
	update();
	emit AnimationFrameChanged(frameIndex);
}

void ViewerWidget::SelectAnimation(int index)
{
	m_animationPlayer.LoadAnimation(m_animationManager.GetAnimation(index));
	update();
}

void ViewerWidget::Pause()
{
	m_animationPlayer.Pause();
}

void ViewerWidget::Stop()
{
	m_animationPlayer.Stop();
}

void ViewerWidget::SeekForward()
{
	m_animationPlayer.SeekForward();
}

void ViewerWidget::SeekBackward()
{
	m_animationPlayer.SeekBackward();
}

void ViewerWidget::JumpToEnd()
{
	m_animationPlayer.JumpToEnd();
}

void ViewerWidget::JumpToStart()
{
	m_animationPlayer.JumpToStart();
}

void ViewerWidget::PlayForward(bool loop, int fps)
{
	m_animationPlayer.SetFramesPerSecond(fps);
	m_animationPlayer.PlayForward(loop);
}

void ViewerWidget::PlayReverse(bool loop, int fps)
{
	m_animationPlayer.SetFramesPerSecond(fps);
	m_animationPlayer.PlayReverse(loop);
}
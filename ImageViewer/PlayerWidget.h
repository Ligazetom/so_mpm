#pragma once
#include <qwidget.h>
#include "ui_PlayerWidget.h"
#include <QList>
#include "Animation.h"
#include "AnimationPlayer.h"
#include "AnimationManager.h"

class PlayerWidget :
	public QWidget
{
	Q_OBJECT

public:
	PlayerWidget(QWidget* parent = Q_NULLPTR);

	inline bool Loop() const { return ui->checkBoxLoop->isChecked(); }
	inline int GetFps() const { return ui->spinBoxSpeed->value(); }
	inline void ClearComboBoxAnimationName() { ui->comboBoxAnimationName->clear(); }
	
private:
	Ui::PlayerWidget* ui;
	
	void ResetAnimationControls();
	void ResetAnimationButtons();	
	void SetPlayerButtonsState(bool enabled);
	void SetPlayerButtonsCheckedState(int buttonsState);

private slots:
	void on_spinBoxCurrentFrame_valueChanged(int val);
	void on_horizontalSliderCurrentFrame_sliderMoved(int val);
	void on_comboBoxAnimationName_currentIndexChanged(int val);
	
	void on_pushButtonJumpBackward_pressed();
	void on_pushButtonJumpForward_pressed();
	void on_pushButtonPlayForward_pressed();
	void on_pushButtonPlayReverse_pressed();
	void on_pushButtonSeekBackward_pressed();
	void on_pushButtonSeekForward_pressed();
	void on_pushButtonStop_pressed();

public slots:
	void SetupComboBoxAnimationName(const AnimationManager* manager);
	void SetupAnimationControls(const AnimationPlayer* player);
	void SetCurrentFrame(int frame);
	void OnAnimationEnded();

signals:
	void SpinBoxCurrentFrameValueChanged(int);
	void HorizontalSliderFrameValueChanged(int);
	void ComboBoxAnimationNameIndexChanged(int);
	void PushButtonJumpForwardPressed();
	void PushButtonJumpBackwardPressed();
	void PushButtonPlayForwardPressed();
	void PushButtonPlayReversePressed();
	void PushButtonSeekForwardPressed();
	void PushButtonSeekBackwardPressed();
	void PushButtonStopPressed();
	void PausePressed();
};


#include "MaskSettingsDialog.h"


MaskSettingsDialog::MaskSettingsDialog(QWidget* parent) : QDialog(parent), ui(new Ui::MaskSettingsDialog)
{
	ui->setupUi(this);
	m_radius = 2;
	SetupMaskSpinBoxes();
}

void MaskSettingsDialog::on_spinBoxMaskRadius_valueChanged(int newVal)
{
	m_radius = newVal;
	SetupMaskSpinBoxes();
}

void MaskSettingsDialog::SetupMaskSpinBoxes()
{
	RemoveOldSpinBoxes();
	m_spinBoxArr.reserve(2 * static_cast<size_t>(m_radius) + 1);		

	for (int i = 0; i < 2 * m_radius + 1; i++)
	{
		for (int j = 0; j < 2 * m_radius + 1; j++)
		{
			m_spinBoxArr.emplace_back(new QDoubleSpinBox);
			m_spinBoxArr.back()->setMinimum(-100.);
			m_spinBoxArr.back()->setMaximum(100.);
			m_spinBoxArr.back()->setGeometry(0, 0, 10, 5);
			m_spinBoxArr.back()->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
			ui->gridLayout->addWidget(m_spinBoxArr.back(), i, j, 1, 1, Qt::AlignCenter);
		}
	}
}

void MaskSettingsDialog::RemoveOldSpinBoxes()
{
	for (size_t i = 0; i < m_spinBoxArr.size(); i++)
	{
		ui->gridLayout->removeWidget(m_spinBoxArr[i]);
	}

	for (size_t i = 0; i < m_spinBoxArr.size(); i++)
	{
		m_spinBoxArr[i]->deleteLater();
	}

	m_spinBoxArr.clear();
}

void MaskSettingsDialog::on_pushButtonSet_pressed()
{
	FillMaskValues();
	close();
}

void MaskSettingsDialog::on_pushButtonDiscard_pressed()
{
	close();
}

void MaskSettingsDialog::FillMaskValues()
{
	std::vector<double> vals;

	double multiplier = static_cast<double>(ui->spinBoxMaskNumerator->value()) / static_cast<double>(ui->spinBoxMaskDenominator->value());
	m_mask.SetRadius(m_radius);
	vals.reserve(static_cast<size_t>(m_spinBoxArr.size()));

	for (size_t i = 0; i < m_spinBoxArr.size(); i++)
	{
		vals.emplace_back(m_spinBoxArr[i]->value() * multiplier);
	}

	m_mask.SetMaskValues(std::move(vals));
}
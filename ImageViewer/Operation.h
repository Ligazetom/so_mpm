#pragma once

#include "NImage.h"
#include <qdebug.h>
#include "Mask.h"
#include "GeneralTypes.h"
#include <cmath>
#include <iostream>
#include <algorithm>
#include <fstream>


class Operation
{
public:
	virtual void Do(void *operationParams) = 0;

protected:
	Operation();
	void ResizeOutputImageIfNeeded(const NImage& from, NImage& to) const;
	void ResizeOutputImageIfNeeded(const QImage& from, NImage& to) const;
	void ResizeOutputImageIfNeeded(const QImage& from, QImage& to) const;
	double Mean(const NImage& img) const;
	double SumNeighbourValues(int row, int column, const NImage& img) const;
	double SumNeighbourValuesWithMultiplication(int row, int column, const NImage& img, const PixelGradients& grads) const;
	double L2NormSquared(const NImage& img) const;
	PixelGradients GradientsNormSquaredAt(int row, int col, const NImage& from) const;
	double SumGradients(const PixelGradients& grads) const;
};

/***************************************/
//
//
//			FSHS
//
//
/***************************************/

class OperationFSHS : public Operation 
{
public: 
	OperationFSHS();
	
	virtual void Do(void *operationParams);

private:
	const Histogram* m_histogram = nullptr;
	double FindMinimumIntensity() const;
	double FindMaximumIntensity() const;
};

/***************************************/
//
//
//			Color-inversion
//
//
/***************************************/

class OperationInvertColors : public Operation
{
public:
	OperationInvertColors();

	void Do(void *operationParams);
};

class Threshold : public Operation
{
public:
	virtual void Do(void *operationParams) = 0;

protected:
	Threshold();
};

/***************************************/
//
//
//			BERNSEN	
//
//
/***************************************/

class ThresholdBernsen : public Threshold
{
public:
	ThresholdBernsen();
	void Do(void *operationParams);
};

/***************************************/
//
//
//			ISODATA
//
//
/***************************************/

class ThresholdISODATA : public Threshold
{
public:
	ThresholdISODATA();
	void Do(void *operationParams);
private:
	const Histogram *m_histogram = nullptr;
	size_t Count(uchar a, uchar b) const;
	double Mean(uchar a, uchar b) const;
};

/***************************************/
//
//
//			FLAT
//
//
/***************************************/

class ThresholdFLAT : public Threshold 
{
public:
	ThresholdFLAT();
	void Do(void *operationParams);
};


/***************************************/
//
//
//			CONVOLUSION
//
//
/***************************************/

class OperationConvolution : public Operation
{
public:
	OperationConvolution();
	void Do(void* operationParams);

};

/***************************************/
//
//
//			CLEAR
//
//
/***************************************/

class OperationClear : public Operation
{
public:
	OperationClear();
	void Do(void* operationParams);
};

/***************************************/
//
//
//			LHE_EXPLICIT
//
//
/***************************************/

class OperationLHEExplicit : public Operation
{
public:
	OperationLHEExplicit();
	void Do(void* operationParams);

private:
	NImage m_iterInput;
	NImage m_iterResult;
	double m_tau;

	void DoIteration();
};

/***************************************/
//
//
//			LHE_IMPLICIT
//
//
/***************************************/

class OperationLHEImplicit : public Operation
{
public:
	OperationLHEImplicit();
	void Do(void* operationParams);

private:
	NImage m_iterInput;
	NImage m_gaussSeidelVar;
	NImage m_sorIterResult;
	NImage m_iterResult;
	
	double m_tau;
	double m_tol;
	double m_omega;
	double m_ap;
	double m_aq;
	double m_res = std::numeric_limits<double>::max();

	void GaussSeidel();
	void DoIteration();
	void SOR();
	void DoSORIteration();
};

/***************************************/
//
//
//			PERONA_MALIK_BASIC
//
//
/***************************************/

class OperationPeronaMalikBasic : public Operation
{
public:
	OperationPeronaMalikBasic();
	void Do(void* operationParams);

private:
	NImage m_iterInput;
	NImage m_iterResult;
	NImage m_edgeDetection;
	Animation* m_edgeAnimTo = nullptr;
	double m_tau;
	double m_K;

	void DoIteration();
	void ApplyGFunction(PixelGradients& grads) const;
};

/***************************************/
//
//
//			PERONA_MALIK_REGULARIZED
//
//
/***************************************/

class OperationPeronaMalikRegularized : public Operation
{
public:
	OperationPeronaMalikRegularized();
	void Do(void* operationParams);

private:
	NImage m_iterInput;
	NImage m_gaussSeidelVar;
	NImage m_sorIterResult;
	NImage m_iterResult;
	NImage m_edgeDetection;
	std::vector<PixelGradients> m_gradients;
	Animation* m_edgeAnimTo = nullptr;

	double m_K;
	double m_tau;
	double m_tol;
	double m_omega;
	double m_ap;
	double m_res = std::numeric_limits<double>::max();
	double m_tauImplicit;
	double m_tolImplicit;
	double m_omegaImplicit;
	size_t m_numOfStepsImplicit;

	void ApplyGFunction();
	void ComputeGradientsForImage(const NImage& img);
	void GaussSeidel();
	void DoIteration();
	void SOR();
	void DoSORIteration();
};

/***************************************/
//
//
//			CURVATURE FILTER
//
//
/***************************************/

class OperationCurvatureFilter : public Operation
{
public:
	OperationCurvatureFilter();
	void Do(void* operationParams);

private:
	NImage m_iterInput;
	NImage m_gaussSeidelVar;
	NImage m_sorIterResult;
	NImage m_iterResult;
	Animation* m_edgeAnimTo = nullptr;
	std::vector<PixelGradients> m_edgeGradients;
	std::vector<double> m_volumetricGradients;

	int m_sorIterationResidualCheck;
	double m_epsilon;
	double m_tau;
	double m_tol;
	double m_omega;
	double m_ap;
	double m_aq;
	double m_res = std::numeric_limits<double>::max();

	void InvertGradientNormsValues(PixelGradients& grads) const;
	void RegularizeGradientNorms(PixelGradients& grads) const;
	double ComputeVolumetricGradientNorm(const PixelGradients& grads) const;
	void ComputeGradientsForImage(const NImage& img);
	void GaussSeidel();
	void DoIteration();
	void SOR();
	void DoSORIteration();
};


/***************************************/
//
//
//			GMCF
//
//
/***************************************/


class OperationGMCF : public Operation
{
public:
	OperationGMCF();
	void Do(void* operationParams);

private:
	NImage m_iterInput;
	NImage m_gaussSeidelVar;
	NImage m_sorIterResult;
	NImage m_iterResult;
	Animation* m_edgeAnimTo = nullptr;
	std::vector<PixelGradients> m_edgeGradientsRegularized;
	std::vector<PixelGradients> m_edgeGradientsSmoothed;
	std::vector<PixelGradients> m_edgeGradientsDivided;
	std::vector<double> m_volumetricGradients;

	int m_sorIterationResidualCheck;
	double m_epsilon;
	double m_tau;
	double m_tol;
	double m_omega;
	double m_ap;
	double m_aq;
	double m_res = std::numeric_limits<double>::max();
	double m_K;
	size_t m_numOfStepsImplicit;
	double m_omegaImplicit;
	double m_tauImplicit;
	double m_tolImplicit;

	void InvertGradientNormsValues(PixelGradients& grads) const;
	void RegularizeGradientNorms(PixelGradients& grads) const;
	double ComputeVolumetricGradientNorm(const PixelGradients& grads) const;
	void ComputeGradientsForImage(const NImage& img);
	void GaussSeidel();
	void DoIteration();
	void SOR();
	void DoSORIteration();
};


/***************************************/
//
//
//			DISTANCE FUNCTION
//
//
/***************************************/

class OperationDistanceFunction : public Operation
{
public:
	OperationDistanceFunction();
	void Do(void* operationParams);

private:
	NImage m_df;
	Animation* m_edgeAnimTo = nullptr;
	std::vector<PixelGradients> m_edgeGradients;
	std::vector<double> m_volumetricGradients;

	void PrepareValues();
	const double TOL = 0.0001;
	const size_t MAX_ITER = 500000;
	int m_timeStepCount;
};
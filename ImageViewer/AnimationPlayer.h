#pragma once

#include <qobject.h>
#include "Animation.h"
#include <QImage>
#include <QTimer>

class AnimationPlayer :
	public QObject
{
	Q_OBJECT
public:
	AnimationPlayer(QObject* parent = Q_NULLPTR);

	void LoadAnimation(const Animation* animation);
	void UnloadAnimation();
	void SetOutput(const QImage** img);
	void PlayForward(bool loop);
	void PlayReverse(bool loop);
	void SetFramesPerSecond(size_t fps);	
	void JumpToStart();
	void JumpToEnd();
	void SeekForward();
	void SeekBackward();
	void Pause();
	void Stop();
	inline bool IsInLoop() const { return m_loop; }
	inline size_t GetFps() const { return m_fps; }
	inline size_t GetCurrentFrameIndex() const { return m_currentFrameIndex; }
	inline size_t GetAnimationLength() const { return m_animation->Length(); }
	inline bool HasAnimation() const { return static_cast<bool>(m_animation); }
	inline const QString& GetAnimationName() const { return m_animation->GetName(); }

private:
	const Animation* m_animation = nullptr;
	const QImage** m_output = nullptr;
	int m_currentFrameIndex = 0;
	int m_animationLength = 0;
	size_t m_fps = 1;
	QTimer m_timer;
	bool m_loop = false;
	bool m_isForward = true;

	void StopTimer();
	void StartTimer(double timeDiff);
	void PlayFrame();
	void DecrementFrame();
	void IncrementFrame();

private slots:
	void OnTimerTimeout();

public slots:
	void SetFrameIndex(int frameIndex);


signals:
	void FrameChanged(int);
	void AnimationEnded();
	void AnimationStarted();
	void AnimationPaused();
	void AnimationStopped();
	void AnimationLoaded();
	void AnimationUnloaded();
};


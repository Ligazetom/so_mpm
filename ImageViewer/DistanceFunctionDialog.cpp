#include "DistanceFunctionDialog.h"

DistanceFunctionDialog::DistanceFunctionDialog(QWidget* parent) : QDialog(parent), ui(new Ui::DistanceFunctionDialog)
{
	ui->setupUi(this);
}

void DistanceFunctionDialog::CopyParameters()
{
	m_params.params.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_params.params.numOfSteps = 1;
	m_params.params.omega = ui->doubleSpinBoxOmega->value();
	m_params.params.tolerance = ui->doubleSpinBoxTolerance->value();
	m_params.params.epsilon = GetEpsilon();
	m_params.params.sorIterationResidualCheck = ui->spinBoxSORIterationCheck->value();
	m_params.numOfIterations = ui->spinBoxIterations->value();
}

double DistanceFunctionDialog::GetEpsilon() const
{
	double temp(ui->doubleSpinBoxEpsilon->value());

	for (int i = 0; i < abs(ui->spinBoxExponent->value()); i++)
	{
		temp /= 10.;
	}

	return temp;
}

void DistanceFunctionDialog::on_spinBoxSORIterationCheck_valueChanged(int val)
{
	QString text;

	switch (val)
	{
	case 1:
	{
		text = QString("st iteration.");
		break;
	}
	case 2:
	{
		text = QString("nd iteration.");
		break;
	}
	case 3:
	{
		text = QString("rd iteration.");
		break;
	}
	default:
		text = QString("th iteration.");
	}

	ui->labelSORCheck->setText(text);
}

void DistanceFunctionDialog::on_pushButtonOK_pressed()
{
	CopyParameters();
	emit settingsAccepted(m_params);
	accept();
}

void DistanceFunctionDialog::on_pushButtonCancel_pressed()
{
	reject();
}

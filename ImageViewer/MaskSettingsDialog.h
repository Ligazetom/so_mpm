#pragma once

#include <QDialog.h>
#include "ui_MaskSettingsDialog.h"
#include <qspinbox.h>
#include <qlayout.h>
#include <vector>
#include "Mask.h"

class MaskSettingsDialog :
	public QDialog
{
	Q_OBJECT

public:
	MaskSettingsDialog(QWidget* parent = Q_NULLPTR);
	
	inline const Mask& GetMask() const { return m_mask; }
	inline int GetMaskRadius() const { return m_radius; }
	inline bool IsMaskSet() const { return m_mask.IsMaskSet(); }

private:
	Ui::MaskSettingsDialog* ui;
	std::vector<QDoubleSpinBox*> m_spinBoxArr;
	int m_radius;
	Mask m_mask;

	void SetupMaskSpinBoxes();
	void RemoveOldSpinBoxes();
	void FillMaskValues();

private slots:
	void on_spinBoxMaskRadius_valueChanged(int newVal);
	void on_pushButtonSet_pressed();
	void on_pushButtonDiscard_pressed();
};


#include "PeronaMalikBasicDialog.h"

PeronaMalikBasicDialog::PeronaMalikBasicDialog(QWidget* parent) : QDialog(parent), ui(new Ui::PeronaMalikBasicDialog)
{
	ui->setupUi(this);
}

void PeronaMalikBasicDialog::CopyParameters()
{
	m_params.createAnimation = ui->checkBoxCreateAnimation->isChecked();
	m_params.saveEveryNthFrame = ui->spinBoxFrameDensity->value();
	m_params.animationName = ui->lineEditAnimationName->text();
	m_params.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_params.numOfSteps = static_cast<size_t>(ceil(ui->doubleSpinBoxTime->value() / m_params.timeStep));
	m_params.K = ui->doubleSpinBoxK->value();
	m_params.visualizeEdgeDetector = m_params.createAnimation && ui->checkBoxVisualizeEdgeDetector->isChecked();
}

void PeronaMalikBasicDialog::on_doubleSpinBoxTimeStep_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(ui->doubleSpinBoxTime->value() / val))));
}

void PeronaMalikBasicDialog::on_doubleSpinBoxTime_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(val / ui->doubleSpinBoxTimeStep->value()))));
}

void PeronaMalikBasicDialog::on_checkBoxCreateAnimation_toggled(bool toggle)
{
	ui->groupBoxAnimationSettings->setEnabled(toggle);
}

void PeronaMalikBasicDialog::on_spinBoxFrameDensity_valueChanged(int val)
{
	QString text;

	switch (val)
	{
	case 1:
	{
		text = QString("st frame.");
		break;
	}
	case 2:
	{
		text = QString("nd frame.");
		break;
	}
	case 3:
	{
		text = QString("rd frame.");
		break;
	}
	default:
		text = QString("th frame.");
	}

	ui->labelFrameDensity->setText(text);
}

void PeronaMalikBasicDialog::on_pushButtonOK_pressed()
{
	CopyParameters();
	emit settingsAccepted(m_params);
	accept();
}

void PeronaMalikBasicDialog::on_pushButtonCancel_pressed()
{
	reject();
}

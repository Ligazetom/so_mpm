#include "DialogManager.h"

DialogManager::DialogManager(QObject* parent)
{

}

void DialogManager::MaskSettings()
{
	m_maskSettingsDialog.exec();	
}

void DialogManager::OperationConvolution(ViewerWidget* connector)
{
	OperationConvolutionParams params(m_maskSettingsDialog.GetMask());

	connector->OperationConvolution(params);
	connector->OperationConfirmed();
}

void DialogManager::ThresholdISODATA(ViewerWidget* connector)
{
	ThresholdISODATAParams params;

	connector->ThresholdISODATA(params);
	connector->OperationConfirmed();
}

void DialogManager::ThresholdBernsen(ViewerWidget* connector)
{
	m_thresholdBernsenDialog = new ThresholdBernsenDialog();

	connect(m_thresholdBernsenDialog, SIGNAL(settingsAccepted(ThresholdBernsenParams&)), connector, SLOT(ThresholdBernsen(ThresholdBernsenParams&)));
	connect(m_thresholdBernsenDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_thresholdBernsenDialog->exec();
	m_thresholdBernsenDialog->deleteLater();
}

void DialogManager::ThresholdFLAT(ViewerWidget* connector)
{
	m_thresholdFLATDialog = new ThresholdFLATDialog();

	connect(m_thresholdFLATDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));
	connect(m_thresholdFLATDialog, SIGNAL(rejected()), connector, SLOT(RefreshFromHistory()));
	connect(m_thresholdFLATDialog, SIGNAL(ThresholdValueChanged(ThresholdFLATParams&)), connector, SLOT(ThresholdFLAT(ThresholdFLATParams&)));

	ThresholdFLATParams init;
	init.thresholdValue = 127;
	connector->ThresholdFLAT(init);

	m_thresholdFLATDialog->exec();
	m_thresholdFLATDialog->deleteLater();
}

void DialogManager::InvertColors(ViewerWidget* connector)
{
	OperationInvertColorsParams params;

	connector->OperationInvertColors(params);
	connector->OperationConfirmed();
}

void DialogManager::FSHS(ViewerWidget* connector)
{
	OperationFSHSParams params;

	connector->OperationFSHS(params);
	connector->OperationConfirmed();
}

void DialogManager::Clear(ViewerWidget* connector)
{
	OperationClearParams params;

	connector->OperationClear(params);
	connector->OperationConfirmed();
}

void DialogManager::LHEExplicit(ViewerWidget* connector)
{
	m_lheExplicitDialog = new LHEExplicitDialog();

	connect(m_lheExplicitDialog, SIGNAL(settingsAccepted(OperationLHEExplicitParams&)), connector, SLOT(OperationLHEExplicit(OperationLHEExplicitParams&)));
	connect(m_lheExplicitDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_lheExplicitDialog->exec();
	m_lheExplicitDialog->deleteLater();
}

void DialogManager::LHEImplicit(ViewerWidget* connector)
{
	m_lheImplicitDialog = new LHEImplicitDialog();

	connect(m_lheImplicitDialog, SIGNAL(settingsAccepted(OperationLHEImplicitParams&)), connector, SLOT(OperationLHEImplicit(OperationLHEImplicitParams&)));
	connect(m_lheImplicitDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_lheImplicitDialog->exec();
	m_lheImplicitDialog->deleteLater();
}

void DialogManager::PeronaMalikBasic(ViewerWidget* connector)
{
	m_peronaMalikBasicDialog = new PeronaMalikBasicDialog();

	connect(m_peronaMalikBasicDialog, SIGNAL(settingsAccepted(OperationPeronaMalikBasicParams&)), connector, SLOT(OperationPeronaMalikBasic(OperationPeronaMalikBasicParams&)));
	connect(m_peronaMalikBasicDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_peronaMalikBasicDialog->exec();
	m_peronaMalikBasicDialog->deleteLater();
}

void DialogManager::PeronaMalikRegularized(ViewerWidget* connector)
{
	m_peronaMalikRegularizedDialog = new PeronaMalikRegularizedDialog();

	connect(m_peronaMalikRegularizedDialog, SIGNAL(settingsAccepted(OperationPeronaMalikRegularizedParams&)), connector, SLOT(OperationPeronaMalikRegularized(OperationPeronaMalikRegularizedParams&)));
	connect(m_peronaMalikRegularizedDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_peronaMalikRegularizedDialog->exec();
	m_peronaMalikRegularizedDialog->deleteLater();
}

void DialogManager::CurvatureFilter(ViewerWidget* connector)
{
	m_curvatureFilterDialog = new CurvatureFilterDialog();

	connect(m_curvatureFilterDialog, SIGNAL(settingsAccepted(OperationCurvatureFilterParams&)), connector, SLOT(OperationCurvatureFilter(OperationCurvatureFilterParams&)));
	connect(m_curvatureFilterDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_curvatureFilterDialog->exec();
	m_curvatureFilterDialog->deleteLater();
}

void DialogManager::GMCF(ViewerWidget* connector)
{
	m_GMCFDialog = new GMCFDialog();

	connect(m_GMCFDialog, SIGNAL(settingsAccepted(OperationGMCFParams&)), connector, SLOT(OperationGMCF(OperationGMCFParams&)));
	connect(m_GMCFDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_GMCFDialog->exec();
	m_GMCFDialog->deleteLater();
}

void DialogManager::DistanceFunction(ViewerWidget* connector)
{
	m_distanceFunctionDialog = new DistanceFunctionDialog();

	connect(m_distanceFunctionDialog, SIGNAL(settingsAccepted(OperationDistanceFunctionParams&)), connector, SLOT(OperationDistanceFunction(OperationDistanceFunctionParams&)));
	connect(m_distanceFunctionDialog, SIGNAL(accepted()), connector, SLOT(OperationConfirmed()));

	m_distanceFunctionDialog->exec();
	m_distanceFunctionDialog->deleteLater();
}
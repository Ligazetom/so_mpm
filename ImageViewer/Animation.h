#pragma once

#include "NImage.h"
#include <QImage>
#include <QList>
#include <QString>

class Animation
{
public:
	Animation(const QString& name);
	Animation(const QList<QImage>& animation);

	const QImage* GetFrame(int index) const;
	QImage* AddFrame();
	void AddFrame(const QImage& frame);
	void AddFrame(const NImage& frame);
	void Clear();
	int Length() const;
	const QString& GetName() const;
	void SetName(const QString& name);
	void Reserve(size_t numOfFrames);

private:
	QString m_name = "";
	QList<QImage> m_frames;
	int m_numOfFrames = 0;
};


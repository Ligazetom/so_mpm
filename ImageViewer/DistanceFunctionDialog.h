#pragma once

#include <qdialog.h>
#include "ui_DistanceFunctionDialog.h"
#include <QMessageBox>
#include "GeneralTypes.h"

class DistanceFunctionDialog : public QDialog
{
	Q_OBJECT

public:
	DistanceFunctionDialog(QWidget* parent = Q_NULLPTR);

private:
	Ui::DistanceFunctionDialog* ui;
	OperationDistanceFunctionParams m_params;
	QMessageBox m_msgBox;

	void CopyParameters();
	double GetEpsilon() const;

private slots:
	void on_spinBoxSORIterationCheck_valueChanged(int val);
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();

signals:
	void settingsAccepted(OperationDistanceFunctionParams&);
};
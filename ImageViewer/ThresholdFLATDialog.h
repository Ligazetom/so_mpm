#pragma once
#include <qdialog.h>

#include "ui_ThresholdFLATDialog.h"
#include "GeneralTypes.h"

class ThresholdFLATDialog :
	public QDialog
{
	Q_OBJECT

public:
	ThresholdFLATDialog(QWidget* parent = Q_NULLPTR);

	inline int GetThresholdValue() const { return ui->spinBoxThreshold->value(); }

private:
	Ui::ThresholdFLATDialog* ui;

private slots:
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();
	void on_spinBoxThreshold_valueChanged(int val);

signals:
	void ThresholdValueChanged(ThresholdFLATParams&);
};


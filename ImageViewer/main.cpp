#include "ImageViewer.h"
#include <QtWidgets/QApplication>

int main(int argc, char* argv[])
{
	QLocale::setDefault(QLocale::c());

	QCoreApplication::setOrganizationName("MPM");
	QCoreApplication::setApplicationName("ImageViewer");

	QApplication a(argc, argv);
	a.setAttribute(Qt::AA_DisableWindowContextHelpButton);
	ImageViewer w;
	w.show();
	return a.exec();
}
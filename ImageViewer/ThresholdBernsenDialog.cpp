#include "ThresholdBernsenDialog.h"

ThresholdBernsenDialog::ThresholdBernsenDialog(QWidget* parent) : QDialog(parent), ui(new Ui::ThresholdBernsenDialog)
{
	ui->setupUi(this);
}

void ThresholdBernsenDialog::on_pushButtonOK_pressed()
{
	CopyParameters();

	emit settingsAccepted(m_params);
	accept();
}

void ThresholdBernsenDialog::on_pushButtonCancel_pressed()
{
	reject();
}

void ThresholdBernsenDialog::CopyParameters()
{
	m_params.isBackgroundDark = static_cast<bool>(ui->comboBoxBackground->currentIndex());
	m_params.minContrast = ui->doubleSpinBoxMinContrast->value();
	m_params.radius = ui->spinBoxRadius->value();
	m_params.useCircularMask = ui->checkBoxUseCircularMask->isChecked();
}
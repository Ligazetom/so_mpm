#include "Animation.h"

Animation::Animation(const QString& name)
{
	m_name = name;
}

Animation::Animation(const QList<QImage>& animation)
{
	m_numOfFrames = animation.size();
	m_frames = animation;
}

QImage* Animation::AddFrame()
{
	m_frames.push_back(QImage());
	m_numOfFrames = m_frames.size();

	return &(m_frames[m_frames.size() - 1]);
}

void Animation::AddFrame(const NImage& frame)
{
	m_frames.push_back(frame.GetQImage());
	m_numOfFrames = m_frames.size();
}

void Animation::AddFrame(const QImage& frame)
{
	m_frames.push_back(frame);
	m_numOfFrames = m_frames.size();
}

void Animation::Clear()
{
	m_frames.clear();
	m_numOfFrames = m_frames.size();
}

int Animation::Length() const
{
	return m_numOfFrames;
}

const QImage* Animation::GetFrame(int index) const 
{
	return &(m_frames[index]);
}

const QString& Animation::GetName() const
{
	return m_name;
}

void Animation::SetName(const QString& name)
{
	m_name = name;
}

void Animation::Reserve(size_t numOfFrames)
{
	m_frames.reserve(numOfFrames);
}
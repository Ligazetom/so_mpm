#include "ThresholdFLATDialog.h"

ThresholdFLATDialog::ThresholdFLATDialog(QWidget* parent) : QDialog(parent), ui(new Ui::ThresholdFLATDialog)
{
	ui->setupUi(this);

	connect(ui->spinBoxThreshold, SIGNAL(valueChanged(int)), ui->horizontalSliderThreshold, SLOT(setValue(int)));
	connect(ui->horizontalSliderThreshold, SIGNAL(sliderMoved(int)), ui->spinBoxThreshold, SLOT(setValue(int)));
}

void ThresholdFLATDialog::on_pushButtonOK_pressed()
{
	accept();
}

void ThresholdFLATDialog::on_pushButtonCancel_pressed()
{
	reject();
}

void ThresholdFLATDialog::on_spinBoxThreshold_valueChanged(int val)
{
	ThresholdFLATParams params;

	params.thresholdValue = static_cast<uchar>(val);

	emit ThresholdValueChanged(params);
}
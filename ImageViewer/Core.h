#pragma once

#include <qimage.h>
#include <qdebug.h>
#include <vector>
#include "Mask.h"
#include "NImage.h"
#include "Operation.h"
#include "GeneralTypes.h"
#include "LimitedHistory.h"

constexpr int MAX_UNDO_HISTORY = 4;
constexpr double CLEAR_BACKGROUND_VALUE = 1.;

class Core
{
public:
	Core();
	Core(const QImage& img);
	~Core();

	const QImage* SetImage(const QImage& img);
	const QImage* NewImage(const QSize& imgSize);
	const QImage& GetOriginalImg() const;
	QImage* GetShowImg();	
	void ResetToOriginal();

	void PushShowedImgToHistory();
	void Clear();

	void RefreshFromHistory();
	bool CanNextUndo();
	bool CanNextRedo();
	bool Undo();
	bool Redo();

	void ApplyOperationConvolution(OperationConvolutionParams& params);
	void ApplyThresholdBernsen(ThresholdBernsenParams& params);
	void ApplyThresholdFLAT(ThresholdFLATParams& params);
	void ApplyThresholdISODATA(ThresholdISODATAParams& params);
	void ApplyOperationFSHS(OperationFSHSParams& params);
	void ApplyOperationInvertColors(OperationInvertColorsParams& params);
	void ApplyOperationClear(OperationClearParams& params);
	void ApplyOperationLHEExplicit(OperationLHEExplicitParams& params);
	void ApplyOperationLHEImplicit(OperationLHEImplicitParams& params);
	void ApplyOperationPeronaMalikBasic(OperationPeronaMalikBasicParams& params);
	void ApplyOperationPeronaMalikRegularized(OperationPeronaMalikRegularizedParams& params);
	void ApplyOperationCurvatureFilter(OperationCurvatureFilterParams& params);
	void ApplyOperationGMCF(OperationGMCFParams& params);
	void ApplyOperationDistanceFunction(OperationDistanceFunctionParams& params);


private:
	Histogram m_histogram;
	QImage m_originalImg;
	QImage m_showImg;
	NImage m_resultImg;
	LimitedHistory<QImage> m_history;

	void ApplyOperation(OPERATION_TYPE type, void* params);
	void ComputeHistogram(const QImage* img);
	void UpdateShowImage();
	void Initialize();
};


#include "LHEExplicitDialog.h"

LHEExplicitDialog::LHEExplicitDialog(QWidget* parent) : QDialog(parent), ui(new Ui::LHEExplicitDialog)
{
	ui->setupUi(this);
}

void LHEExplicitDialog::CopyParameters()
{
	m_params.createAnimation = ui->checkBoxCreateAnimation->isChecked();
	m_params.saveEveryNthFrame = ui->spinBoxFrameDensity->value();
	m_params.animationName = ui->lineEditAnimationName->text();
	m_params.timeStep = ui->doubleSpinBoxTimeStep->value();
	m_params.numOfSteps = static_cast<size_t>(ceil(ui->doubleSpinBoxTime->value() / m_params.timeStep));
}

bool LHEExplicitDialog::IsStabilityConditionMet()
{
	return ui->doubleSpinBoxTimeStep->value() <= 0.25;
}

void LHEExplicitDialog::ShowMessageWarning()
{
	m_msgBox.setText("Current time step can cause instability of algorithm!");
	m_msgBox.setIcon(QMessageBox::Warning);
	m_msgBox.exec();
}

void LHEExplicitDialog::on_doubleSpinBoxTimeStep_valueChanged(double val)
{
	if (!IsStabilityConditionMet()) ShowMessageWarning();

	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(ui->doubleSpinBoxTime->value() / val))));
}

void LHEExplicitDialog::on_doubleSpinBoxTime_valueChanged(double val)
{
	ui->labelIterationCount->setText(QString("Iteration count: %1").arg(static_cast<int>(ceil(val/ ui->doubleSpinBoxTimeStep->value()))));
}

void LHEExplicitDialog::on_checkBoxCreateAnimation_toggled(bool toggle)
{
	ui->groupBoxAnimationSettings->setEnabled(toggle);
}

void LHEExplicitDialog::on_spinBoxFrameDensity_valueChanged(int val)
{
	QString text;

	switch (val)
	{
	case 1:
	{
		text = QString("st frame.");
		break;
	}
	case 2:
	{
		text = QString("nd frame.");
		break;
	}
	case 3:
	{
		text = QString("rd frame.");
		break;
	}
	default:
		text = QString("th frame.");
	}

	ui->labelFrameDensity->setText(text);
}

void LHEExplicitDialog::on_pushButtonOK_pressed()
{
	CopyParameters();
	emit settingsAccepted(m_params);
	accept();
}

void LHEExplicitDialog::on_pushButtonCancel_pressed()
{
	reject();
}
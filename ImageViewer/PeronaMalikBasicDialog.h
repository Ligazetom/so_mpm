#pragma once

#include <qdialog.h>
#include "ui_PeronaMalikBasicDialog.h"
#include <QMessageBox>
#include "GeneralTypes.h"

class PeronaMalikBasicDialog : public QDialog
{
	Q_OBJECT

public:
	PeronaMalikBasicDialog(QWidget* parent = Q_NULLPTR);

private:
	Ui::PeronaMalikBasicDialog* ui;
	OperationPeronaMalikBasicParams m_params;
	QMessageBox m_msgBox;

	void CopyParameters();

private slots:
	void on_doubleSpinBoxTimeStep_valueChanged(double val);
	void on_doubleSpinBoxTime_valueChanged(double val);
	void on_checkBoxCreateAnimation_toggled(bool toggle);
	void on_spinBoxFrameDensity_valueChanged(int val);
	void on_pushButtonOK_pressed();
	void on_pushButtonCancel_pressed();

signals:
	void settingsAccepted(OperationPeronaMalikBasicParams&);
};
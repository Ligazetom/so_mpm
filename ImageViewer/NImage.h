#pragma once

#include <qimage.h>
#include <qrect.h>
#include <qdebug.h>
#include "Mask.h"
#include <fstream>

class NImage
{
public:
	NImage();
	NImage(const QImage *img);
	NImage(const QImage &img);
	NImage(int height, int width, double val = 0);
	NImage(int height, int width, const double* arr);
	NImage(const NImage &img);						
	NImage(NImage &&img);
	NImage(const QString& path);
	static NImage FromQImageKeepValues(const QImage *img);
	NImage& operator=(const NImage &img);
	NImage& operator=(NImage &&img);
	NImage operator+(const NImage& img) const;
	NImage operator-(const NImage& img) const;
	NImage operator*(double val) const;
	NImage operator/(double val) const;
	double* operator[](int index);
	const double* operator[](int index) const;
	~NImage();

	void SetAllValuesTo(double val);
	void ConvertToQImage(QImage* img) const;
	QImage GetQImage() const;
	double* Ptr(int row = 0);
	const double* Ptr(int row = 0) const;
	double* Bits();
	const double* Bits() const;
	NImage Mirrored(bool horizontally = false, bool vertically = false);
	void Mirror(bool horizontally = false, bool vertically = false);
	NImage Copy(const QRect& rect) const;
	void MirrorEdges(int edgeWidth);
	NImage MirroredEdges(int edgeWidth) const;
	void RemoveEdges();
	NImage RemovedEdges() const;
	int GetMirrorEdgeWidth() const;
	int GetUnMirroredRowStart() const;
	int GetUnMirroredColumnStart() const;
	int GetUnMirroredRowEnd() const;
	int GetUnMirroredColumnEnd() const;
	
	inline int Width() const { return m_width; }
	inline int Height() const { return m_height; }
	inline int GetNumOfPixels() const { return m_width * m_height; }

	static void InsertImage(const NImage& from, NImage& to, int x, int y);

	//multiplies mask values with image values returning their sum
	double ApplyMaskWithMultiplicationAt(int row, int col, const Mask& mask);			
	//replaces values in mask equal to 1. with values from image
	void ApplyMaskWithValuesExtractionAt(int row, int col, Mask& mask);					

private:
	double *m_bits;
	double **m_rowStart;
	int m_height;
	int m_width;
	int m_mirroredEdgeWidth = 0;

	void SetupRowPtrs();
	void MirrorHorizontally();
	void MirrorVertically();
};


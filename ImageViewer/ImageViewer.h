#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ImageViewer.h"
#include "ViewerWidget.h"
#include "NewImageDialog.h"
#include "GeneralTypes.h"
#include "DialogManager.h"
#include "AnimationPlayer.h"
#include "AnimationManager.h"
#include "PlayerWidget.h"

class ImageViewer : public QMainWindow
{
	Q_OBJECT

public:
	ImageViewer(QWidget* parent = Q_NULLPTR);

private:
	Ui::ImageViewerClass* ui;
	NewImageDialog* newImgDialog;
	DialogManager m_dialogManager;
	ViewerWidget* m_lastViewerWidget = nullptr;
	PlayerWidget* m_playerWidget = nullptr;

	QSettings settings;
	QMessageBox m_msgBox;

	int TabCount() const;

	//ViewerWidget functions
	ViewerWidget* getViewerWidget(int tabId);
	ViewerWidget* getCurrentViewerWidget();

	//Event filters
	bool eventFilter(QObject* obj, QEvent* event);

	//ViewerWidget Events
	bool ViewerWidgetEventFilter(QObject* obj, QEvent* event);
	void ViewerWidgetWheel(ViewerWidget* w, QEvent* event);

	//ImageViewer Events
	void closeEvent(QCloseEvent* event);

	//Image functions
	void openNewTabForImg(ViewerWidget* vW);
	bool openImage(QString filename);
	bool saveImage(QString filename);
	
	//Rest
	inline bool isImgOpened() const { return static_cast<bool>(ui->tabWidget->count()); }
	void InformNoImageOpened();
	void UpdateGUI(int);
	void UpdatePlayerWidget(int index);

private slots:
	//Tabs slots
	void on_tabWidget_tabCloseRequested(int tabId);
	void on_actionRename_triggered();
	void OnTabsSwitched(int currentTabIndex);
	void HandleAfterOperationSetup();

	//Image slots
	void on_actionNew_triggered();
	void newImageAccepted();
	void on_actionOpen_triggered();
	void on_actionSave_as_triggered();

	void on_actionMask_settings_triggered();
	void on_actionClear_triggered();
	void on_actionInvert_colors_triggered();
	void on_actionConvolutionApply_triggered();
	void on_actionThreshold_ISODATA_triggered();
	void on_actionThreshold_Bernsen_triggered();
	void on_actionThreshold_FLAT_triggered();
	void on_actionFSHS_triggered();
	void on_actionLHE_Explicit_triggered();
	void on_actionLHE_Implicit_triggered();
	void on_actionPeronaMalikBasic_triggered();
	void on_actionPeronaMalikRegularized_triggered();
	void on_actionCurvatureFilter_triggered();
	void on_actionGMCF_triggered();
	void on_actionDistanceFunction_triggered();
	
	void on_actionUndo_triggered();
	void on_actionRedo_triggered();

	void on_actionAnimation_Mode_triggered();
	void on_actionImage_Mode_triggered();

	void OnAnimationFrameChanged(int frameIndex);
	void OnAnimationChanged(int index);
	void OnPlayForward();
	void OnPlayReverse();
	void OnPause();
	void OnStop();
	void OnSeekForward();
	void OnSeekBackward();
	void OnJumpForward();
	void OnJumpBackward();
	void OnUserFrameChanged(int frame);
};

#include "Operation.h"

double gPM(double s, double K)
{
	double ss(sqrt(s) * 255.);

	return 1. / (1. + K * ss * ss);
}


Operation::Operation()
{

}

void Operation::ResizeOutputImageIfNeeded(const NImage& from, NImage& to) const
{
	if (from.Height() == to.Height() && from.Width() == to.Width()) return;

	to = from;
}

void Operation::ResizeOutputImageIfNeeded(const QImage& from, NImage& to) const
{
	if (from.height() == to.Height() && from.width() == to.Width()) return;

	to = NImage(from);
}

void Operation::ResizeOutputImageIfNeeded(const QImage& from, QImage& to) const
{
	if (from.height() == to.height() && from.width() == to.width()) return;

	to = from;
}

double Operation::Mean(const NImage& img) const
{
	double accum = 0.;
	
	for (int i = 0; i < img.GetNumOfPixels(); i++)
	{
		accum += img.Bits()[i];
	}

	return accum / img.GetNumOfPixels();
}

double Operation::SumNeighbourValues(int row, int column, const NImage& img) const
{
	int rowLength = img.Width();
	const double *rowPtr = img.Ptr(row);

	double east = rowPtr[column + 1];
	double north = rowPtr[column - rowLength];
	double west = rowPtr[column - 1];
	double south = rowPtr[column + rowLength];

	return east + north + west + south;
}

double Operation::SumNeighbourValuesWithMultiplication(int row, int column, const NImage& img, const PixelGradients& grads) const
{
	int rowLength = img.Width();
	const double *rowPtr = img.Ptr(row);

	double east = rowPtr[column + 1] * grads.east;
	double north = rowPtr[column - rowLength] * grads.north;
	double west = rowPtr[column - 1] * grads.west;
	double south = rowPtr[column + rowLength] * grads.south;

	return east + north + west + south;
}

double Operation::L2NormSquared(const NImage& img) const
{
	double norm = 0.;

	for (size_t i = 0; i < img.GetNumOfPixels(); i++)
	{
		norm += img.Bits()[i] * img.Bits()[i];
	}

	return norm;
}

double Operation::SumGradients(const PixelGradients& grads) const
{
	return grads.east + grads.north + grads.south + grads.west;
}

PixelGradients Operation::GradientsNormSquaredAt(int row, int col, const NImage& from) const
{
	PixelGradients gradients;
	double ux, uy;

	ux = from[row][col + 1] - from[row][col];
	uy = (from[row - 1][col] + from[row - 1][col + 1] - from[row + 1][col] - from[row + 1][col + 1]) / 4.;
	gradients.east = ux * ux + uy * uy;

	ux = (from[row - 1][col - 1] + from[row][col - 1] - from[row - 1][col + 1] - from[row][col + 1]) / 4.;
	uy = from[row - 1][col] - from[row][col];
	gradients.north = ux * ux + uy * uy;

	ux = from[row][col - 1] - from[row][col];
	uy = (from[row + 1][col - 1] + from[row + 1][col] - from[row - 1][col - 1] - from[row - 1][col]) / 4.;
	gradients.west = ux * ux + uy * uy;

	ux = (from[row][col + 1] + from[row + 1][col + 1] - from[row][col - 1] - from[row + 1][col - 1]) / 4.;
	uy = from[row + 1][col] - from[row][col];
	gradients.south = ux * ux + uy * uy;
	
	return gradients;
}

/***************************************/
//
//
//			FSHS
//
//
/***************************************/

OperationFSHS::OperationFSHS() : Operation()
{

}

double OperationFSHS::FindMaximumIntensity() const 
{
	for (int i = 255; i >= 0 ; i--)
	{
		if (m_histogram->histogram[i] != 0)
		{
			return i / 255.;
		}
	}

	return 0.;
}

double OperationFSHS::FindMinimumIntensity() const
{
	for (int i = 0; i < 256; i++)
	{
		if (m_histogram->histogram[i] != 0)
		{
			return i / 255.;
		}
	}

	return 0.;
}

void OperationFSHS::Do(void* operationParams)
{
	OperationFSHSParams * params = static_cast<OperationFSHSParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_FSHS)
	{
		qDebug() << "Operation - FSHS: Wrong input parameters!";
		return;
	}

	m_histogram = params->histogram;

	double min = FindMinimumIntensity();
	double max = FindMaximumIntensity();

	for (int i = 0; i < params->to->GetNumOfPixels(); i++)
	{
		params->to->Bits()[i] = 1. / (max - min) * (params->to->Bits()[i] - min);
	}
}

/***************************************/
//
//
//			Color-inversion
//
//
/***************************************/

OperationInvertColors::OperationInvertColors() : Operation()
{

}

void OperationInvertColors::Do(void *operationParams)
{
	OperationInvertColorsParams * params = static_cast<OperationInvertColorsParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_INVERT_COLORS)
	{
		qDebug() << "Threshold - Bernsen: Wrong input parameters!";
		return;
	}

	for (int i = 0; i < params->to->GetNumOfPixels(); i++)
	{
		params->to->Bits()[i] = 1. - params->to->Bits()[i];
	}
}

Threshold::Threshold() : Operation()
{

}


/***************************************/
//
//
//			BERNSEN	
//
//
/***************************************/

ThresholdBernsen::ThresholdBernsen() : Threshold()
{

}

void ThresholdBernsen::Do(void *operationParams)
{
	ThresholdBernsenParams * params = static_cast<ThresholdBernsenParams *>(operationParams);

	if (params->type != OPERATION_TYPE::THRESHOLD_BERNSEN)
	{
		qDebug() << "Threshold - Bernsen: Wrong input parameters!";
		return;
	}

	Mask mainMask;

	if (params->useCircularMask)
	{
		mainMask.MakeCircularMask(params->radius);
	}
	else
	{
		mainMask.SetRadius(params->radius);
		mainMask.SetMaskValuesTo(1.);
	};
	
	NImage from(NImage(params->from).MirroredEdges(params->radius));

	int radius = from.GetMirrorEdgeWidth();
	double q = params->isBackgroundDark ? 1. : 0.;

	int width(from.Width() - radius);
	int height(from.Height() - radius);

	for (int row = radius; row < height; row++)
	{
		double* toRow = params->to->Ptr(row - radius);

		for (int col = radius; col < width; col++)
		{
			Mask tempMask(mainMask);
			
			from.ApplyMaskWithValuesExtractionAt(row, col, tempMask);

			double minIntensity = tempMask.GetPositiveMinimum();
			double maxIntensity = tempMask.GetMaximum();
			double contrast = maxIntensity - minIntensity;

			if (contrast >= params->minContrast)
			{
				toRow[col - radius] = (minIntensity + maxIntensity) / 2.;
			}
			else
			{
				toRow[col - radius] = q;
			}
		}
	}
}



/***************************************/
//
//
//			ISODATA
//
//
/***************************************/

ThresholdISODATA::ThresholdISODATA() : Threshold()
{

}

size_t ThresholdISODATA::Count(uchar a, uchar b) const 
{
	size_t accum = 0;

	for (int i = a; i <= b; i++)
	{
		accum += m_histogram->histogram[i];
	}

	return accum;
}

double ThresholdISODATA::Mean(uchar a, uchar b) const 
{
	size_t numerator = 0;
	size_t denominator = 0;

	for (int i = a; i <= b; i++)
	{
		numerator += i * m_histogram->histogram[i];
		denominator += m_histogram->histogram[i];
	}

	return static_cast<double>(numerator) / static_cast<double>(denominator);
}

void ThresholdISODATA::Do(void *operationParams)
{
	ThresholdISODATAParams * params = static_cast<ThresholdISODATAParams *>(operationParams);

	if (params->type != OPERATION_TYPE::THRESHOLD_ISODATA)
	{
		qDebug() << "Threshold - ISODATA: Wrong input parameters!";
		return;
	}

	m_histogram = params->histogram;

	uchar K = 255;
	uchar q = static_cast<uchar>(round(Mean(0, K)));
	uchar oldQ = q / 2 + 1;								//hlavne nech je ina

	while (q != oldQ)
	{
		uchar integerQ = static_cast<uchar>(round(q));
		size_t n0 = Count(0, integerQ);
		size_t n1 = Count(integerQ + 1, K);

		if (n0 == 0 || n1 == 0) return;

		double mi0 = Mean(0, integerQ);
		double mi1 = Mean(integerQ + 1, K);

		oldQ = q;
		q = static_cast<uchar>(round((mi0 + mi1) / 2.));
	}

	ThresholdFLATParams flatParams;

	flatParams.thresholdValue = q;
	flatParams.to = params->to;

	ThresholdFLAT thresholdFlat;

	thresholdFlat.Do(&flatParams);
}


/***************************************/
//
//
//			FLAT
//
//
/***************************************/


ThresholdFLAT::ThresholdFLAT() : Threshold()
{

}

void ThresholdFLAT::Do(void *operationParams)
{
	ThresholdFLATParams * params = static_cast<ThresholdFLATParams *>(operationParams);

	if (params->type != OPERATION_TYPE::THRESHOLD_FLAT)
	{
		qDebug() << "Threshold - FLAT: Wrong input parameters!";
		return;
	}

	for (int i = 0; i < params->to->GetNumOfPixels(); i++)
	{
		uchar converted = static_cast<uchar>(round(params->to->Bits()[i] * 255.));

		params->to->Bits()[i] = converted >= params->thresholdValue ? 1. : 0.;
	}
}

/***************************************/
//
//
//			CONVOLUSION
//
//
/***************************************/

OperationConvolution::OperationConvolution()
{

}

void OperationConvolution::Do(void* operationParams)
{
	OperationConvolutionParams * params = static_cast<OperationConvolutionParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_CONVOLUTION)
	{
		qDebug() << "Operation - Convolution: Wrong input parameters!";
		return;
	}

	int radius = params->mask.GetRadius();

	NImage from(NImage(params->from).MirroredEdges(radius));

	int width(from.Width() - radius);
	int height(from.Height() - radius);

	for (int row = radius; row < height; row++)
	{
		double* toRow = params->to->Ptr(row - radius);

		for (int col = radius; col < width; col++)
		{
			toRow[col - radius] = from.ApplyMaskWithMultiplicationAt(row, col, params->mask);
		}
	}
}

/***************************************/
//
//
//			CLEAR
//
//
/***************************************/

OperationClear::OperationClear()
{

}

void OperationClear::Do(void* operationParams)
{
	OperationClearParams * params = static_cast<OperationClearParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_CLEAR)
	{
		qDebug() << "Operation - Clear: Wrong input parameters!";
		return;
	}

	params->to->SetAllValuesTo(params->clearValue);
}


/***************************************/
//
//
//			LHE_EXPLICIT
//
//
/***************************************/


OperationLHEExplicit::OperationLHEExplicit() : Operation()
{

}

void OperationLHEExplicit::DoIteration()
{
	int radius = 1;
	int width(m_iterInput.Width() - radius);
	int height(m_iterInput.Height() - radius);
	double upCoef = 1 - m_tau * 4;
	double uqCoef = m_tau;

	for (int row = radius; row < height; row++)
	{
		double* fromRow = m_iterInput.Ptr(row);
		double* toRow = m_iterResult.Ptr(row - radius);		

		for (int col = radius; col < width; col++)
		{
			toRow[col - radius] = upCoef * fromRow[col] + uqCoef * SumNeighbourValues(row, col, m_iterInput);
		}
	}
}

void OperationLHEExplicit::Do(void* operationParams)
{
	OperationLHEExplicitParams * params = static_cast<OperationLHEExplicitParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_LHE_EXPLICIT)
	{
		qDebug() << "Operation - LHE_Explicit: Wrong input parameters!";
		return;
	}

	if (params->createAnimation)
	{
		params->animTo->Reserve(params->numOfSteps + 1);
		params->animTo->AddFrame(*params->from);
	}

	m_tau = params->timeStep;
	m_iterInput = NImage(params->from).MirroredEdges(1);
	double lastMean = Mean(m_iterInput);
	m_iterResult = NImage(params->from->height(), params->from->width());

	for (int i = 0; i < params->numOfSteps; i++)
	{
		DoIteration();
		m_iterInput = m_iterResult.MirroredEdges(1);
		double newMean = Mean(m_iterInput);

		//qDebug() << "Old: " << lastMean << " new: " << newMean;

		if (static_cast<int>(newMean * 1000) != static_cast<int>(lastMean * 1000))
		{
			qDebug() << "Means are different";
		}

		lastMean = newMean;

		if (params->createAnimation)
		{
			if (i % params->saveEveryNthFrame == 0)	m_iterResult.ConvertToQImage(params->animTo->AddFrame());
		}
	}

	*(params->to) = m_iterResult;
}


/***************************************/
//
//
//			LHE_IMPLICIT
//
//
/***************************************/

OperationLHEImplicit::OperationLHEImplicit() : Operation()
{

}

void OperationLHEImplicit::GaussSeidel()
{
	for (size_t row = 1; row < m_gaussSeidelVar.Height() - 1; row++)
	{
		double* rowPtr = m_gaussSeidelVar.Ptr(row);
		double* uOldTimeRow = m_iterInput.Ptr(row);

		for (size_t col = 1; col < m_gaussSeidelVar.Width() - 1; col++)
		{
			rowPtr[col] = (uOldTimeRow[col] + SumNeighbourValues(row, col, m_gaussSeidelVar) * m_aq) / m_ap;
		}
	}
}

void OperationLHEImplicit::DoSORIteration()
{
	GaussSeidel();

	m_sorIterResult = m_sorIterResult + (m_gaussSeidelVar - m_sorIterResult) * m_omega;
	m_sorIterResult.RemoveEdges();
	m_sorIterResult.MirrorEdges(1);
}

void OperationLHEImplicit::SOR()
{
	while (m_res > m_tol * m_tol)
	{
		DoSORIteration();

		NImage residual(m_sorIterResult.RemovedEdges());

		for (size_t row = 1; row < m_sorIterResult.Height() - 1; row++)
		{
			double* resRowPtr = residual.Ptr(row - 1);
			const double* sorRowPtr = m_sorIterResult.Ptr(row);
			const double* lastIterRowPtr = m_iterInput.Ptr(row);

			for (size_t col = 1; col < m_sorIterResult.Width() - 1; col++)
			{
				resRowPtr[col - 1] = m_ap * sorRowPtr[col] - m_aq * SumNeighbourValues(row, col, m_sorIterResult) - lastIterRowPtr[col];
			}
		}

		m_res = L2NormSquared(residual);
		m_gaussSeidelVar = m_sorIterResult;
	}

	m_res = std::numeric_limits<double>::max();
}

void OperationLHEImplicit::DoIteration()
{
	SOR();

	m_iterResult = m_sorIterResult.RemovedEdges();
}

void OperationLHEImplicit::Do(void* operationParams)
{
	OperationLHEImplicitParams * params = static_cast<OperationLHEImplicitParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_LHE_IMPLICIT)
	{
		qDebug() << "Operation - LHE_Implicit: Wrong input parameters!";
		return;
	}

	if (params->createAnimation)
	{
		params->animTo->Reserve(params->numOfSteps + 1);
		params->animTo->AddFrame(*params->from);
	}

	m_tol = params->tolerance;
	m_omega = params->omega;
	m_tau = params->timeStep;
	m_ap = 1 + 4 * m_tau;
	m_aq = m_tau;
	m_iterInput = NImage(params->from).MirroredEdges(1);
	m_gaussSeidelVar = m_iterInput;
	m_sorIterResult = m_iterInput;
	m_iterResult = NImage(params->from->height(), params->from->width());
	double lastMean = Mean(m_iterInput);

	for (int i = 0; i < params->numOfSteps; i++)
	{
		DoIteration();
		m_iterInput = m_iterResult.MirroredEdges(1);
		m_gaussSeidelVar = m_iterInput;
		m_sorIterResult = m_iterInput;
	
		double newMean = Mean(m_iterInput);
		//qDebug() << "Old: " << lastMean << " new: " << newMean;

		if (static_cast<int>(newMean * 1000) != static_cast<int>(lastMean * 1000))
		{
			qDebug() << "Means are different";
		}

		lastMean = newMean;

		if (params->createAnimation)
		{
			if (i % params->saveEveryNthFrame == 0)	m_iterResult.ConvertToQImage(params->animTo->AddFrame());
		}
	}

	*(params->to) = m_iterResult;
}

/***************************************/
//
//
//			PERONA_MALIK_BASIC
//
//
/***************************************/

OperationPeronaMalikBasic::OperationPeronaMalikBasic() : Operation()
{

}

void OperationPeronaMalikBasic::ApplyGFunction(PixelGradients& grads) const
{
	grads.east = gPM(grads.east, m_K);
	grads.north = gPM(grads.north, m_K);
	grads.west = gPM(grads.west, m_K);
	grads.south = gPM(grads.south, m_K);
}

void OperationPeronaMalikBasic::DoIteration()
{
	int radius = 1;
	int width(m_iterInput.Width() - radius);
	int height(m_iterInput.Height() - radius);

	size_t iter = 0;

	for (int row = radius; row < height; row++)
	{
		double* fromRow = m_iterInput.Ptr(row);
		double* toRow = m_iterResult.Ptr(row - radius);

		for (int col = radius; col < width; col++)
		{
			PixelGradients grads(GradientsNormSquaredAt(row, col, m_iterInput));
			ApplyGFunction(grads);

			if (m_edgeAnimTo != nullptr)
			m_edgeDetection.Bits()[iter] = SumGradients(grads) / 4.;

			toRow[col - radius] = (1 - m_tau * SumGradients(grads)) * fromRow[col] + m_tau * SumNeighbourValuesWithMultiplication(row, col, m_iterInput, grads);
			iter++;
		}
	}

	if (m_edgeAnimTo != nullptr)
	m_edgeDetection.ConvertToQImage(m_edgeAnimTo->AddFrame());
}

void OperationPeronaMalikBasic::Do(void* operationParams)
{
	OperationPeronaMalikBasicParams * params = static_cast<OperationPeronaMalikBasicParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_PERONA_MALIK_BASIC)
	{
		qDebug() << "Operation - Perona-Malik Basic: Wrong input parameters!";
		return;
	}

	if (params->createAnimation)
	{
		params->animTo->Reserve(params->numOfSteps + 1);
		params->animTo->AddFrame(*params->from);

		if (params->visualizeEdgeDetector)
		{
			params->edgeAnimTo->Reserve(params->numOfSteps + 1);
			m_edgeAnimTo = params->edgeAnimTo;
		}
	}

	m_edgeDetection = NImage(params->from);
	m_K = params->K;
	m_tau = params->timeStep;
	m_iterInput = NImage(params->from).MirroredEdges(1);
	m_iterResult = NImage(params->from->height(), params->from->width());
	double lastMean = Mean(m_iterInput);

	for (int i = 0; i < params->numOfSteps; i++)
	{
		DoIteration();
		m_iterInput = m_iterResult.MirroredEdges(1);

		double newMean = Mean(m_iterInput);
		//qDebug() << "Old: " << lastMean << " new: " << newMean;

		if (static_cast<int>(newMean * 1000) != static_cast<int>(lastMean * 1000))
		{
			qDebug() << "Means are different";
		}

		lastMean = newMean;

		if (params->createAnimation)
		{
			if (i % params->saveEveryNthFrame == 0)	m_iterResult.ConvertToQImage(params->animTo->AddFrame());
		}
	}

	*(params->to) = m_iterResult;
}

/***************************************/
//
//
//			PERONA_MALIK_REGULARIZED
//
//
/***************************************/

OperationPeronaMalikRegularized::OperationPeronaMalikRegularized() : Operation()
{

}

void OperationPeronaMalikRegularized::ApplyGFunction()
{
	for (size_t i = 0; i < m_gradients.size(); i++)
	{
		m_gradients[i].east = gPM(m_gradients[i].east, m_K);
		m_gradients[i].north = gPM(m_gradients[i].north, m_K);
		m_gradients[i].west = gPM(m_gradients[i].west, m_K);
		m_gradients[i].south = gPM(m_gradients[i].south, m_K);

		if (m_edgeAnimTo != nullptr)
		m_edgeDetection.Bits()[i] = SumGradients(m_gradients[i]) / 4.;
	}

	if (m_edgeAnimTo != nullptr)
	m_edgeDetection.ConvertToQImage(m_edgeAnimTo->AddFrame());
}

void OperationPeronaMalikRegularized::ComputeGradientsForImage(const NImage& img)
{
	m_gradients.clear();
	m_gradients.reserve((img.Height() - 2 * img.GetMirrorEdgeWidth()) * (img.Width() - 2 * img.GetMirrorEdgeWidth()));

	for (size_t row = 1; row < img.Height() - 1; row++)
	{
		for (size_t col = 1; col < img.Width() - 1; col++)
		{
			m_gradients.push_back(GradientsNormSquaredAt(row, col, img));
		}
	}

	ApplyGFunction();
}

void OperationPeronaMalikRegularized::GaussSeidel()
{
	size_t iter = 0;

	for (size_t row = 1; row < m_gaussSeidelVar.Height() - 1; row++)
	{
		double* rowPtr = m_gaussSeidelVar.Ptr(row);
		double* uOldTimeRow = m_iterInput.Ptr(row);

		for (size_t col = 1; col < m_gaussSeidelVar.Width() - 1; col++)
		{
			m_ap = 1 + m_tau * SumGradients(m_gradients[iter]);
			rowPtr[col] = (uOldTimeRow[col] + m_tau * SumNeighbourValuesWithMultiplication(row, col, m_gaussSeidelVar, m_gradients[iter])) / m_ap;
			iter++;
		}
	}
}

void OperationPeronaMalikRegularized::DoSORIteration()
{
	GaussSeidel();

	m_sorIterResult = m_sorIterResult + (m_gaussSeidelVar - m_sorIterResult) * m_omega;
	m_sorIterResult.RemoveEdges();
	m_sorIterResult.MirrorEdges(1);
}

void OperationPeronaMalikRegularized::SOR()
{
	while (m_res > m_tol * m_tol)
	{
		DoSORIteration();

		NImage residual(m_sorIterResult.RemovedEdges());
		size_t iter = 0;

		for (size_t row = 1; row < m_sorIterResult.Height() - 1; row++)
		{
			double* resRowPtr = residual.Ptr(row - 1);
			const double* sorRowPtr = m_sorIterResult.Ptr(row);
			const double* lastIterRowPtr = m_iterInput.Ptr(row);

			for (size_t col = 1; col < m_sorIterResult.Width() - 1; col++)
			{
				m_ap = 1 + m_tau * SumGradients(m_gradients[iter]);
				resRowPtr[col - 1] = m_ap * sorRowPtr[col] - m_tau * SumNeighbourValuesWithMultiplication(row, col, m_sorIterResult, m_gradients[iter]) - lastIterRowPtr[col];
				iter++;
			}
		}

		m_res = L2NormSquared(residual);
		m_gaussSeidelVar = m_sorIterResult;
	}

	m_res = std::numeric_limits<double>::max();
}

void OperationPeronaMalikRegularized::DoIteration()
{
	OperationLHEImplicit operation;
	OperationLHEImplicitParams params;
	QImage temp(m_gaussSeidelVar.GetQImage());

	params.createAnimation = false;
	params.from = &temp;
	params.to = &m_gaussSeidelVar;
	params.numOfSteps = m_numOfStepsImplicit;
	params.omega = m_omegaImplicit;
	params.timeStep = m_tauImplicit;
	params.tolerance = m_tolImplicit;

	operation.Do(&params);
	
	ComputeGradientsForImage(m_gaussSeidelVar);
	SOR();

	m_iterResult = m_sorIterResult.RemovedEdges();
}

void OperationPeronaMalikRegularized::Do(void* operationParams)
{
	OperationPeronaMalikRegularizedParams * params = static_cast<OperationPeronaMalikRegularizedParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_PERONA_MALIK_REGULARIZED)
	{
		qDebug() << "Operation - Perona-Malik Regularized: Wrong input parameters!";
		return;
	}

	if (params->createAnimation)
	{
		params->animTo->Reserve(params->numOfSteps + 1);
		params->animTo->AddFrame(*params->from);

		if (params->visualizeEdgeDetector)
		{
			params->edgeAnimTo->Reserve(params->numOfSteps + 1);
			m_edgeAnimTo = params->edgeAnimTo;
		}
	}

	m_tol = params->tolerance;
	m_omega = params->omega;
	m_tau = params->timeStep;
	m_tolImplicit = params->toleranceImplicit;
	m_omegaImplicit = params->omegaImplicit;
	m_tauImplicit = params->timeStepImplicit;
	m_K = params->K;
	m_numOfStepsImplicit = params->numOfStepsImplicit;

	m_edgeDetection = NImage(params->from);
	m_iterInput = NImage(params->from).MirroredEdges(1);
	m_gaussSeidelVar = m_iterInput;
	m_sorIterResult = m_iterInput;
	m_iterResult = NImage(params->from->height(), params->from->width());
	double lastMean = Mean(m_iterInput);

	for (int i = 0; i < params->numOfSteps; i++)
	{
		DoIteration();
		m_iterInput = m_iterResult.MirroredEdges(1);
		m_gaussSeidelVar = m_iterInput;
		m_sorIterResult = m_iterInput;

		double newMean = Mean(m_iterInput);
		//qDebug() << "Old: " << lastMean << " new: " << newMean;

		if (static_cast<int>(newMean * 1000) != static_cast<int>(lastMean * 1000))
		{
			qDebug() << "Means are different";
		}

		lastMean = newMean;

		if (params->createAnimation)
		{
			if (i % params->saveEveryNthFrame == 0)	m_iterResult.ConvertToQImage(params->animTo->AddFrame());
		}
	}

	*(params->to) = m_iterResult;
}

/***************************************/
//
//
//			CURVATURE FILTER
//
//
/***************************************/

OperationCurvatureFilter::OperationCurvatureFilter() : Operation()
{

}

void OperationCurvatureFilter::RegularizeGradientNorms(PixelGradients& grads) const
{
	grads.east = sqrt(grads.east + m_epsilon * m_epsilon);
	grads.north = sqrt(grads.north + m_epsilon * m_epsilon);
	grads.west = sqrt(grads.west + m_epsilon * m_epsilon);
	grads.south = sqrt(grads.south + m_epsilon * m_epsilon);
}

double OperationCurvatureFilter::ComputeVolumetricGradientNorm(const PixelGradients& grads) const
{
	return SumGradients(grads) / 4.;	
}

void OperationCurvatureFilter::InvertGradientNormsValues(PixelGradients& grads) const
{
	grads.east = 1. / grads.east;
	grads.north = 1. / grads.north;
	grads.west = 1. / grads.west;
	grads.south = 1. / grads.south;
}

void OperationCurvatureFilter::ComputeGradientsForImage(const NImage& img)
{
	int numOfPixels = (img.Height() - 2 * img.GetMirrorEdgeWidth()) * (img.Width() - 2 * img.GetMirrorEdgeWidth());
	m_volumetricGradients.clear();
	m_volumetricGradients.reserve(numOfPixels);
	m_edgeGradients.clear();
	m_edgeGradients.reserve(numOfPixels);

	for (size_t row = 1; row < img.Height() - 1; row++)
	{
		for (size_t col = 1; col < img.Width() - 1; col++)
		{
			PixelGradients temp(GradientsNormSquaredAt(row, col, img));
			RegularizeGradientNorms(temp);
			m_volumetricGradients.push_back(ComputeVolumetricGradientNorm(temp));
			InvertGradientNormsValues(temp);
			m_edgeGradients.push_back(temp);
		}
	}
}

void OperationCurvatureFilter::GaussSeidel()
{
	size_t iter = 0;

	for (size_t row = 1; row < m_gaussSeidelVar.Height() - 1; row++)
	{
		double* rowPtr = m_gaussSeidelVar.Ptr(row);
		double* uOldTimeRow = m_iterInput.Ptr(row);

		for (size_t col = 1; col < m_gaussSeidelVar.Width() - 1; col++)
		{
			m_aq = m_tau * m_volumetricGradients[iter] * SumNeighbourValuesWithMultiplication(row, col, m_gaussSeidelVar, m_edgeGradients[iter]);
			m_ap = 1 + m_tau * m_volumetricGradients[iter] * SumGradients(m_edgeGradients[iter]);

			rowPtr[col] = (uOldTimeRow[col] + m_aq) / m_ap;
			iter++;
		}
	}
}

void OperationCurvatureFilter::DoSORIteration()
{
	GaussSeidel();

	m_sorIterResult = m_sorIterResult + (m_gaussSeidelVar - m_sorIterResult) * m_omega;
	m_sorIterResult.RemoveEdges();
	m_sorIterResult.MirrorEdges(1);
}

void OperationCurvatureFilter::SOR()
{
	size_t sorIter = 0;

	while (m_res > m_tol * m_tol)
	{
		DoSORIteration();
		sorIter++;

		if (sorIter % m_sorIterationResidualCheck == 0)
		{
			NImage residual(m_sorIterResult.RemovedEdges());
			size_t iter = 0;

			for (size_t row = 1; row < m_sorIterResult.Height() - 1; row++)
			{
				double* resRowPtr = residual.Ptr(row - 1);
				const double* sorRowPtr = m_sorIterResult.Ptr(row);
				const double* lastIterRowPtr = m_iterInput.Ptr(row);

				for (size_t col = 1; col < m_sorIterResult.Width() - 1; col++)
				{
					m_aq = m_tau * m_volumetricGradients[iter] * SumNeighbourValuesWithMultiplication(row, col, m_sorIterResult, m_edgeGradients[iter]);
					m_ap = 1 + m_tau * m_volumetricGradients[iter] * SumGradients(m_edgeGradients[iter]);

					resRowPtr[col - 1] = m_ap * sorRowPtr[col] - m_aq - lastIterRowPtr[col];
					iter++;
				}
			}

			m_res = L2NormSquared(residual);
		}

		m_gaussSeidelVar = m_sorIterResult;
	}

	std::cout << "Num of SOR iterations: " << sorIter << std::endl;
	m_res = std::numeric_limits<double>::max();
}

void OperationCurvatureFilter::DoIteration()
{
	ComputeGradientsForImage(m_gaussSeidelVar);

	if (m_edgeAnimTo != nullptr)
	NImage(m_iterResult.Height(), m_iterResult.Width(), &(m_volumetricGradients[0])).ConvertToQImage(m_edgeAnimTo->AddFrame());

	SOR();

	m_iterResult = m_sorIterResult.RemovedEdges();
}

void OperationCurvatureFilter::Do(void* operationParams)
{
	OperationCurvatureFilterParams * params = static_cast<OperationCurvatureFilterParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_CURVATURE_FILTER)
	{
		qDebug() << "Operation - Curvature filter: Wrong input parameters!";
		return;
	}

	if (params->createAnimation)
	{
		params->animTo->Reserve(params->numOfSteps + 1);
		params->animTo->AddFrame(*params->from);

		if (params->visualizeEdgeDetector)
		{
			params->edgeAnimTo->Reserve(params->numOfSteps + 1);
			m_edgeAnimTo = params->edgeAnimTo;
		}
	}

	m_tau = params->timeStep;
	m_epsilon = params->epsilon;
	m_tol = params->tolerance;
	m_omega = params->omega;
	m_sorIterationResidualCheck = params->sorIterationResidualCheck;

	if (params->Nfrom == nullptr)
	{
		m_iterInput = NImage(params->from).MirroredEdges(1);
	}
	else
	{
		m_iterInput = (*params->Nfrom).MirroredEdges(1);
	}

	m_gaussSeidelVar = m_iterInput;
	m_sorIterResult = m_iterInput;

	if (params->Nfrom == nullptr)
	{
		m_iterResult = NImage(params->from);
	}
	else
	{
		m_iterResult = *params->Nfrom;
	}

	for (int i = 0; i < params->numOfSteps; i++)
	{
		DoIteration();
		m_iterInput = m_iterResult.MirroredEdges(1);
		m_gaussSeidelVar = m_iterInput;
		m_sorIterResult = m_iterInput;

		if (params->createAnimation)
		{
			if (i % params->saveEveryNthFrame == 0)	m_iterResult.ConvertToQImage(params->animTo->AddFrame());
		}
	}

	*(params->to) = m_iterResult;
}

/***************************************/
//
//
//			GMCF
//
//
/***************************************/

OperationGMCF::OperationGMCF() : Operation()
{

}

void OperationGMCF::RegularizeGradientNorms(PixelGradients& grads) const
{
	grads.east = sqrt(grads.east + m_epsilon * m_epsilon);
	grads.north = sqrt(grads.north + m_epsilon * m_epsilon);
	grads.west = sqrt(grads.west + m_epsilon * m_epsilon);
	grads.south = sqrt(grads.south + m_epsilon * m_epsilon);
}

double OperationGMCF::ComputeVolumetricGradientNorm(const PixelGradients& grads) const
{
	return SumGradients(grads) / 4.;
}

void OperationGMCF::InvertGradientNormsValues(PixelGradients& grads) const
{
	grads.east = 1. / grads.east;
	grads.north = 1. / grads.north;
	grads.west = 1. / grads.west;
	grads.south = 1. / grads.south;
}

void OperationGMCF::ComputeGradientsForImage(const NImage& img)
{
	int numOfPixels = (img.Height() - 2 * img.GetMirrorEdgeWidth()) * (img.Width() - 2 * img.GetMirrorEdgeWidth());
	m_volumetricGradients.clear();
	m_volumetricGradients.reserve(numOfPixels);
	m_edgeGradientsRegularized.clear();
	m_edgeGradientsRegularized.reserve(numOfPixels);
	m_edgeGradientsSmoothed.clear();
	m_edgeGradientsSmoothed.reserve(numOfPixels);
	m_edgeGradientsDivided.clear();
	m_edgeGradientsDivided.reserve(numOfPixels);

	OperationLHEImplicit operation;
	OperationLHEImplicitParams params;
	QImage tempFrom(img.GetQImage());
	NImage tempTo(img);

	params.createAnimation = false;
	params.from = &tempFrom;
	params.to = &tempTo;
	params.numOfSteps = m_numOfStepsImplicit;
	params.omega = m_omegaImplicit;
	params.timeStep = m_tauImplicit;
	params.tolerance = m_tolImplicit;

	operation.Do(&params);

	for (size_t row = 1; row < img.Height() - 1; row++)
	{
		for (size_t col = 1; col < img.Width() - 1; col++)
		{
			PixelGradients temp(GradientsNormSquaredAt(row, col, tempTo));
			m_edgeGradientsSmoothed.push_back(temp);

			m_edgeGradientsSmoothed.back().east = gPM(m_edgeGradientsSmoothed.back().east, m_K);
			m_edgeGradientsSmoothed.back().north = gPM(m_edgeGradientsSmoothed.back().north, m_K);
			m_edgeGradientsSmoothed.back().west = gPM(m_edgeGradientsSmoothed.back().west, m_K);
			m_edgeGradientsSmoothed.back().south = gPM(m_edgeGradientsSmoothed.back().south, m_K);
		}
	}

	for (size_t row = 1; row < img.Height() - 1; row++)
	{
		for (size_t col = 1; col < img.Width() - 1; col++)
		{
			PixelGradients temp(GradientsNormSquaredAt(row, col, img));
			RegularizeGradientNorms(temp);
			m_volumetricGradients.push_back(ComputeVolumetricGradientNorm(temp));
			InvertGradientNormsValues(temp);
			m_edgeGradientsRegularized.push_back(temp);
		}
	}

	for (size_t i = 0; i < m_edgeGradientsSmoothed.size(); i++)
	{
		PixelGradients temp(m_edgeGradientsSmoothed[i]);

		temp.east = temp.east * m_edgeGradientsRegularized[i].east;
		temp.north = temp.north * m_edgeGradientsRegularized[i].north;
		temp.west = temp.west * m_edgeGradientsRegularized[i].west;
		temp.south = temp.south * m_edgeGradientsRegularized[i].south;

		m_edgeGradientsDivided.push_back(temp);
	}
}

void OperationGMCF::GaussSeidel()
{
	size_t iter = 0;

	for (size_t row = 1; row < m_gaussSeidelVar.Height() - 1; row++)
	{
		double* rowPtr = m_gaussSeidelVar.Ptr(row);
		double* uOldTimeRow = m_iterInput.Ptr(row);

		for (size_t col = 1; col < m_gaussSeidelVar.Width() - 1; col++)
		{
			m_aq = m_tau * m_volumetricGradients[iter] * SumNeighbourValuesWithMultiplication(row, col, m_gaussSeidelVar, m_edgeGradientsDivided[iter]);
			m_ap = 1 + m_tau * m_volumetricGradients[iter] * SumGradients(m_edgeGradientsDivided[iter]);

			rowPtr[col] = (uOldTimeRow[col] + m_aq) / m_ap;
			iter++;
		}
	}
}

void OperationGMCF::DoSORIteration()
{
	GaussSeidel();

	m_sorIterResult = m_sorIterResult + (m_gaussSeidelVar - m_sorIterResult) * m_omega;
	m_sorIterResult.RemoveEdges();
	m_sorIterResult.MirrorEdges(1);
}

void OperationGMCF::SOR()
{
	size_t sorIter = 0;

	while (m_res > m_tol * m_tol)
	{
		DoSORIteration();
		sorIter++;

		if (sorIter % m_sorIterationResidualCheck == 0)
		{
			NImage residual(m_sorIterResult.RemovedEdges());
			size_t iter = 0;

			for (size_t row = 1; row < m_sorIterResult.Height() - 1; row++)
			{
				double* resRowPtr = residual.Ptr(row - 1);
				const double* sorRowPtr = m_sorIterResult.Ptr(row);
				const double* lastIterRowPtr = m_iterInput.Ptr(row);

				for (size_t col = 1; col < m_sorIterResult.Width() - 1; col++)
				{
					m_aq = m_tau * m_volumetricGradients[iter] * SumNeighbourValuesWithMultiplication(row, col, m_sorIterResult, m_edgeGradientsDivided[iter]);
					m_ap = 1 + m_tau * m_volumetricGradients[iter] * SumGradients(m_edgeGradientsDivided[iter]);

					resRowPtr[col - 1] = m_ap * sorRowPtr[col] - m_aq - lastIterRowPtr[col];
					iter++;
				}
			}

			m_res = L2NormSquared(residual);
		}

		m_gaussSeidelVar = m_sorIterResult;
	}

	std::cout << "Num of SOR iterations: " << sorIter << std::endl;
	m_res = std::numeric_limits<double>::max();
}

void OperationGMCF::DoIteration()
{
	ComputeGradientsForImage(m_gaussSeidelVar);

	if (m_edgeAnimTo != nullptr)
		NImage(m_iterResult.Height(), m_iterResult.Width(), &(m_volumetricGradients[0])).ConvertToQImage(m_edgeAnimTo->AddFrame());

	SOR();

	m_iterResult = m_sorIterResult.RemovedEdges();
}

void OperationGMCF::Do(void* operationParams)
{
	OperationGMCFParams * params = static_cast<OperationGMCFParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_GMCF)
	{
		qDebug() << "Operation - GMCF: Wrong input parameters!";
		return;
	}

	if (params->createAnimation)
	{
		params->animTo->Reserve(params->numOfSteps + 1);
		params->animTo->AddFrame(*params->from);

		if (params->visualizeEdgeDetector)
		{
			params->edgeAnimTo->Reserve(params->numOfSteps + 1);
			m_edgeAnimTo = params->edgeAnimTo;
		}
	}

	m_tau = params->timeStep;
	m_epsilon = params->epsilon;
	m_tol = params->tolerance;
	m_omega = params->omega;
	m_sorIterationResidualCheck = params->sorIterationResidualCheck;

	m_K = params->K;
	m_numOfStepsImplicit = params->numOfStepsImplicit;
	m_omegaImplicit = params->omegaImplicit;
	m_tauImplicit = params->timeStepImplicit;
	m_tolImplicit = params->toleranceImplicit;

	m_iterInput = NImage(params->from).MirroredEdges(1);
	m_gaussSeidelVar = m_iterInput;
	m_sorIterResult = m_iterInput;
	m_iterResult = NImage(params->from->height(), params->from->width());

	for (int i = 0; i < params->numOfSteps; i++)
	{
		DoIteration();
		m_iterInput = m_iterResult.MirroredEdges(1);
		m_gaussSeidelVar = m_iterInput;
		m_sorIterResult = m_iterInput;

		if (params->createAnimation)
		{
			if (i % params->saveEveryNthFrame == 0)	m_iterResult.ConvertToQImage(params->animTo->AddFrame());
		}
	}

	*(params->to) = m_iterResult;
}

/***************************************/
//
//
//			DISTANCE FUNCTION
//
//
/***************************************/

OperationDistanceFunction::OperationDistanceFunction() : Operation()
{

}

void OperationDistanceFunction::PrepareValues()
{
	bool hasZero = false;

	for (int i = 0; i < m_df.GetNumOfPixels(); i++)
	{
		if (m_df.Bits()[i] == 255.)
		{
			m_df.Bits()[i] = -1.;
		}
		else if (m_df.Bits()[i] == 127.)
		{
			m_df.Bits()[i] = 1.;
		}
		else if(m_df.Bits()[i] == 0.)
		{
			hasZero = true;
		}
	}

	if (!hasZero)
	{
		std::cout << "Image has no edge\n";
	}
}

void OperationDistanceFunction::Do(void* operationParams)
{
	OperationDistanceFunctionParams * params = static_cast<OperationDistanceFunctionParams *>(operationParams);

	if (params->type != OPERATION_TYPE::OPERATION_DISTANCE_FUNCTION)
	{
		qDebug() << "Operation - Distance function: Wrong input parameters!";
		return;
	}

	m_timeStepCount = params->numOfIterations;
	m_df = NImage::FromQImageKeepValues(params->from);
	PrepareValues();

	double mass = 10.;
	double taud = 0.4;
	size_t iter = 0;

	while (mass > TOL && iter < MAX_ITER)
	{
		iter++;
		NImage u0(m_df.MirroredEdges(1));
		int width = u0.Width();
		int height = u0.Height();
		mass = 0.;

		for (int i = 1; i < height - 1; i++)
		{
			for (int j = 1; j < width - 1; j++)
			{
				double currentPixel = u0[i][j];

				if (m_df[i - 1][j - 1] != 0.)
				{
					double m0 = std::min(0., u0[i][j-1] - currentPixel);
					double m1 = std::min(0., u0[i][j + 1] - currentPixel);
					double m2 = std::min(0., u0[i - 1][j] - currentPixel);
					double m3 = std::min(0., u0[i + 1][j] - currentPixel);

					m0 *= m0;
					m1 *= m1;
					m2 *= m2;
					m3 *= m3;

					m_df[i - 1][j - 1] = currentPixel + taud - taud * sqrt(std::max(m0, m1) + std::max(m2, m3));
				}

				mass += (currentPixel - m_df[i - 1][j - 1]) * (currentPixel - m_df[i - 1][j - 1]);
			}
		}

		mass = sqrt(mass);
	}

	NImage signedTest = NImage::FromQImageKeepValues(params->from);

	for (int i = 0; i < m_df.GetNumOfPixels(); i++)
	{
		if (signedTest.Bits()[i] == 127.)
		{
			m_df.Bits()[i] *= -1;
		}

		if (std::abs(m_df.Bits()[i]) < TOL)
		{
			m_df.Bits()[i] = 0.;
		}
	}

	NImage out(m_df);
	std::vector<NImage> anim;
	OperationCurvatureFilter operation;
	OperationCurvatureFilterParams pars(params->params);
	pars.createAnimation = false;
	
	anim.reserve(m_timeStepCount);

	for (int i = 0; i < m_timeStepCount; i++)
	{
		NImage in(out);
		pars.Nfrom = &in;
		pars.to = &out;
		operation.Do(&pars);
		anim.push_back(out);
	}

	std::ofstream outstr("results.txt");
	
	outstr << m_df.Height() << std::endl << m_df.Width() << std::endl << m_timeStepCount << std::endl;

	for (size_t i = 0; i < anim.size(); i++)
	{
		const double* bits = anim[i].Bits();

		for (int j = 0; j < anim[i].GetNumOfPixels(); j++)
		{
			outstr << bits[j] << std::endl;
		}
	}

	outstr.close();
}
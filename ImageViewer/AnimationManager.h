#pragma once
#include <qobject.h>

#include <vector>
#include "Animation.h"

class AnimationManager :
	public QObject
{
	Q_OBJECT
public:
	AnimationManager(QObject* parent = Q_NULLPTR);

	Animation* NewAnimation(const QString& animationName = "");
	void PushAnimation(const Animation& animation);
	const Animation* GetAnimation(int index) const;

	inline int GetNumOfAnimations() const { return static_cast<int>(m_animations.size()); }

private:
	std::vector<Animation> m_animations;
};

